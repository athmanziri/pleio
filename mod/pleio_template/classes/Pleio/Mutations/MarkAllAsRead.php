<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class MarkAllAsRead {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "markAllAsRead",
            "inputFields" => [
                "id" => [ "type" => Type::string() ]
            ],
            "outputFields" => [
                "success" => [ "type" => Type::boolean() ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $dbprefix = elgg_get_config("dbprefix");

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            return [ "success" => false ];
        }

        $result = update_data("UPDATE {$dbprefix}notifications SET unread = 'no' WHERE user_guid = {$user->guid}");
        if ($result) {
            return [ "success" => true ];
        }

        return [ "success" => false ];
    }
}
