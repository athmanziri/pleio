<?php
$dbprefix = elgg_get_config("dbprefix");

# check for group featured errors
$group_result = get_data("SELECT * FROM elgg_groups_entity WHERE guid NOT IN (SELECT guid FROM elgg_metadata md left
                        JOIN elgg_metastrings ms ON md.name_id = ms.id LEFT JOIN elgg_groups_entity ge ON md.entity_guid = ge.guid
                        WHERE string = 'isFeatured')");

$errors = FALSE;

if ($group_result) {
    $group_count = count($group_result);
    $errors = TRUE;
} else {
    $group_count = 0;
}

# check for group plugin configuration errors
function is_plugins_on_old_template($group) {
    if ($group->blog_enable == 'yes') {
        return true;
    }
    if ($group->event_manager_enable == 'yes') {
        return true;
    }
    if ($group->forum_enable == 'yes') {
        return true;
    }
    if ($group->questions_enable == 'yes') {
        return true;
    }
    if ($group->file_enable == 'yes') {
        return true;
    }
    if (($group->pages_enable == 'yes' || $group->static_enable == 'yes')) {
        return true;
    }
    if ($group->tasks_enable == 'yes') {
        return true;
    }
    return false;
}


$options = [
    "type" => "group",
    "limit" => false
];
$groups = elgg_get_entities($options);
$groups_plugins_count = 0;

foreach ($groups as $group) {
    if (is_array($groups->plugins)) {
        continue;
    }

    if (is_plugins_on_old_template($group)) {
        $groups_plugins_count++;
        $errors = TRUE;
        continue;
    }
}



# check for wiki errors
$wiki_result = get_data("SELECT guid FROM elgg_entities ee
                         LEFT JOIN elgg_entity_subtypes es ON ee.subtype = es.id
                         LEFT JOIN elgg_metadata md ON ee.guid = md.entity_guid
                         LEFT JOIN elgg_metastrings ms ON md.name_id = ms.id
                         WHERE (ms.string = 'parent_guid' AND es.subtype = 'page') OR es.subtype = 'page_top'");

if ($wiki_result) {
    $wiki_count = count($wiki_result);
    $errors = TRUE;
} else {
    $wiki_count = 0;
}

# check for site static error
$options = [
    "type" => "object",
    "subtypes" => ["static", "static_top"],
    "limit" => false
];
$static_result = elgg_get_entities($options);

if ($static_result) {
    $static_count = count($static_result);
    $errors = TRUE;
} else {
    $static_count = 0;
}

# check for photo album errors
$options = [
    "type" => "object",
    "subtypes" => ["album"],
    "limit" => false
];
$album_result = elgg_get_entities($options);


if ($album_result) {
    $album_count = count($album_result);
    $errors = TRUE;
} else {
    $album_count = 0;
}

# check for photo album image errors
$options = [
    "type" => "object",
    "subtypes" => ["image"],
    "limit" => false
];
$album_image_result = elgg_get_entities($options);


if ($album_image_result) {
    $album_image_count = count($album_image_result);
    $errors = TRUE;
} else {
    $album_image_count = 0;
}
?>


<?php if ($errors): ?>

    <p>Er zijn problemen met de database:</p>

    <?php if ($group_count > 0 || $groups_plugins_count > 0): ?>
        <p>Er zijn <?php echo $group_count; ?> groepen waarvan de aangeraden vlag nog niet bekend is in nieuwe template.</p>
        <p>Er zijn <?php echo $groups_plugins_count; ?> groepen waarvan de plugin configuratie niet is overgenomen.</p>
        <?php echo elgg_view("output/confirmlink", [
            "href" => "/action/admin/upgrade_database_groups",
            "text" => "Update groepen",
            "class" => "elgg-button elgg-button-submit",
            "is_action" => true
        ]); ?> <br><br>
    <?php endif; ?>
    <?php if ($wiki_count > 0): ?>
        <p>Er zijn <?php echo $wiki_count; ?> wiki pagina's welke niet beschikbaar zijn in het nieuwe template.</p>

        <?php echo elgg_view("output/confirmlink", [
            "href" => "/action/admin/upgrade_database_wiki",
            "text" => "Update wiki's",
            "class" => "elgg-button elgg-button-submit",
            "is_action" => true
        ]); ?> <br><br>
    <?php endif; ?>
    <?php if ($album_count > 0): ?>
        <p>Er zijn <?php echo $album_count; ?> fotoalbums met <?php echo $album_image_count; ?> foto's welke niet beschikbaar zijn in het nieuwe template.</p>

        <?php echo elgg_view("output/confirmlink", [
            "href" => "/action/admin/upgrade_database_photoalbums",
            "text" => "Update fotoalbums",
            "class" => "elgg-button elgg-button-submit",
            "is_action" => true
        ]); ?> <br><br>
    <?php endif; ?>

    <?php if ($static_count > 0): ?>
        <p>Er zijn <?php echo $static_count; ?> statische pagina's welke niet beschikbaar zijn in het nieuwe template.</p>

        <?php echo elgg_view("output/confirmlink", [
            "href" => "/action/admin/upgrade_database_static_pages",
            "text" => "Update statische pagina's",
            "class" => "elgg-button elgg-button-submit",
            "is_action" => true
        ]); ?> <br><br>
    <?php endif; ?>

<?php else: ?>
    <p>Database klaar voor nieuwe template</p>
<?php endif; ?>
