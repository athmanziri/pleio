<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class Subgroup extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "fields" => [
                "id" => [ "type" => Type::int() ],
                "name" => [ "type" => Type::string() ],
                "members" => [ "type" => Type::listOf($registry->get("User")) ]
            ]
        ]);
    }
}
