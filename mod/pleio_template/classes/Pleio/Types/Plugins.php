<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class Plugins extends EnumType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Plugins",
            "description" => "The available plugins.",
            "values" => [
                "events" => [ "value" => "events" ],
                "blog" => [ "value" => "blog" ],
                "discussion" => [ "value" => "discussion" ],
                "questions" => [ "value" => "questions" ],
                "files" => [ "value" => "files" ],
                "wiki" => [ "value" => "wiki" ],
                "tasks" => [ "value" => "tasks" ]
            ]
        ]);
    }
}
