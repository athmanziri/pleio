<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddPoll {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "addPoll",
            "inputFields" => [
                "title" => [ "type" => Type::nonNull(Type::string()) ],
                "choices" => [ "type" => Type::nonNull(Type::listOf(Type::string())) ],
                "accessId" => [ "type" => Type::int() ]
            ],
            "outputFields" => [
                "entity" => [
                    "type" => Type::nonNull($registry->get("Entity")),
                    "resolve" => function($entity, array $args, $context, ResolveInfo $info) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        if (!elgg_is_logged_in()) {
            throw new Exception("not_logged_in");
        }

        $entity = new \ElggObject();
        $entity->title = $input["title"];
        $entity->description = $input["description"];
        $entity->subtype = "poll";

        if (isset($input["accessId"])) {
            $entity->access_id = (int) $input["accessId"];
        } else {
            $entity->access_id = get_default_access();
        }

        $result = $entity->save();

        if (!$result) {
            throw new Exception("could_not_save");
        }

        foreach ($input["choices"] as $choice) {
            $choice_entity = new \ElggObject();
            $choice_entity->subtype = "poll_choice";
            $choice_entity->text = $choice;
            $choice_entity->access_id = $entity->access_id;
            $result = $choice_entity->save();

            if ($result) {
                add_entity_relationship($choice_entity->guid, "poll_choice", $entity->guid);
            }
        }

        return [
            "guid" => $entity->guid
        ];
    }
}
