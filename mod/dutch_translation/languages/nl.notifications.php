<?php
$language = array (
  'friends:all' => 'Al je contacten',
  'notifications:subscriptions:personal:description' => 'Ontvang een melding als iemand reageert op of iets toevoegt aan jouw bijdragen of groepen.',
  'notifications:subscriptions:personal:title' => 'Ontvang een melding als iemand op jou reageert',
  'notifications:subscriptions:collections:edit' => 'Om de groepen met je contacten aan te passen, klik hier.',
  'notifications:subscriptions:changesettings' => 'Je meldingen',
  'notifications:subscriptions:changesettings:groups' => 'Meldingen vanuit je groepen',
  'notifications:subscriptions:title' => 'Verander de meldingen die je krijgt van een individuele gebruiker',
  'notifications:subscriptions:groups:description' => 'Hieronder kun je aangeven hoe je een melding wilt ontvangen als er iets wordt toegevoegd aan een groep waar je lid van bent.',
  'notifications:subscriptions:success' => 'Je meldings-instellingen zijn opgeslagen.',
  'notifications:subscriptions:description' => 'Om meldingen te ontvangen wanneer jouw contacten nieuwe content plaatsen: selecteer hieronder een contact en kies de wijze waarop je meldingen wilt ontvangen.',
  'notifications:subscriptions:friends:title' => 'Contacten',
  'notifications:subscriptions:friends:description' => 'Het volgende is een automatische verzameling van je contacten. Om updates te ontvangen selecteer hieronder. Dit zal invloed hebben op de betreffende gebruikers in het hoofd meldingen-instellingen overzicht aan de onderkant van deze pagina.',
);
add_translation("nl", $language);
