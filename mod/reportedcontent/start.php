<?php
/**
 * Elgg Reported content.
 *
 * @package ElggReportedContent
 */

elgg_register_event_handler('init', 'system', 'reportedcontent_init');

/**
 * Initialize the plugin
 */
function reportedcontent_init() {

	// Register a page handler, so we can have nice URLs
	elgg_register_page_handler('reportedcontent', 'reportedcontent_page_handler');

	// Extend CSS
	elgg_extend_view('css/elgg', 'reportedcontent/css');
	elgg_extend_view('css/admin', 'reportedcontent/admin_css');

	// Extend footer with report content link
	if (elgg_is_logged_in()) {
		$href = "javascript:elgg.forward('reportedcontent/add'";
		$href .= "+'?address='+encodeURIComponent(location.href)";
		$href .= "+'&title='+encodeURIComponent(document.title));";

		elgg_register_menu_item('footer', array(
			'name' => 'report_this',
			'href' => $href,
			'title' => elgg_echo('reportedcontent:this:tooltip'),
			'text' => elgg_view_icon('report-this') . elgg_echo('reportedcontent:this'),
			'priority' => 500,
			'section' => 'alt',
		));
	}

	elgg_register_plugin_hook_handler('register', 'menu:user_hover', 'reportedcontent_user_hover_menu');
	elgg_register_plugin_hook_handler('reportedcontent:add', 'system', 'reportedcontent_add_hook');

	// Add admin menu item
	// @todo Might want to move this to a 'feedback' section. something other than utils
	elgg_register_admin_menu_item('administer', 'reportedcontent', 'administer_utilities');

	elgg_register_widget_type(
			'reportedcontent',
			elgg_echo('reportedcontent'),
			elgg_echo('reportedcontent:widget:description'),
			'admin');

	// Register actions
	$action_path = elgg_get_plugins_path() . "reportedcontent/actions/reportedcontent";
	elgg_register_action('reportedcontent/add', "$action_path/add.php");
	elgg_register_action('reportedcontent/delete', "$action_path/delete.php", 'admin');
	elgg_register_action('reportedcontent/archive', "$action_path/archive.php", 'admin');
}

/**
 * Reported content page handler
 *
 * Serves the add report page
 *
 * @param array $page Array of page routing elements
 * @return bool
 */
function reportedcontent_page_handler($page) {
	// only logged in users can report things
	gatekeeper();

	$content .= elgg_view_title(elgg_echo('reportedcontent:this'));
	$content .= elgg_view_form('reportedcontent/add');
	$sidebar = elgg_echo('reportedcontent:instructions');

	$params = array(
		'content' => $content,
		'sidebar' => $sidebar,
	);
	$body = elgg_view_layout('one_sidebar', $params);

	echo elgg_view_page(elgg_echo('reportedcontent:this'), $body);
	return true;
}

/**
 * Add report user link to hover menu
 */
function reportedcontent_user_hover_menu($hook, $type, $return, $params) {
	$user = $params['entity'];

	$profile_url = urlencode($user->getURL());
	$name = urlencode($user->name);
	$url = "reportedcontent/add?address=$profile_url&title=$name";

	if (elgg_is_logged_in() && elgg_get_logged_in_user_guid() != $user->guid) {
		$item = new ElggMenuItem(
				'reportuser',
				elgg_echo('reportedcontent:user'),
				$url);
		$item->setSection('action');
		$return[] = $item;
	}

	return $return;
}

function reportedcontent_add_hook($hook, $type, $return_value, $params){
	if(!empty($params) && is_array($params)){
		$report = elgg_extract("report", $params);
		$site = elgg_get_site_entity();

		// get users with setting
		$options = array(
			"type" => "user",
			"limit" => false,
			"site_guids" => false,
			"private_setting_name" => ELGG_PLUGIN_USER_SETTING_PREFIX . "reportedcontent:notify",
			"private_setting_value" => "yes",

		);

		if($users = elgg_get_entities_from_private_settings($options)){
			$filtered_users = array();

			foreach($users as $user){
				if(($user->getGUID() != $report->getOwner()) && $user->isAdmin()){
					$filtered_users[] = $user;
				}
			}

			if(!empty($filtered_users)){
				$subject = elgg_echo("reportedcontent:usersettings:notify:subject");
				$message = elgg_echo("reportedcontent:usersettings:notify:message", array(
										$report->getOwnerEntity()->name,
										$site->url . "admin/administer_utilities/reportedcontent"));

				foreach($filtered_users as $user){
					notify_user($user->getGUID(), $report->getOwner(), $subject, $message);
				}
			}
		}
	}
}