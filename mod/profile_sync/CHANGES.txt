Version History
===============

x.x:

- added: support for lastrun timestamp in datasource query
- added: option to ban users during sync (this will not sync profile data)
- added: plugin setting to increase PHP memory limit
- added: datasource cache cleaup to help with OOM issues
- added: support for CSV file sync
- added: hourly schedule options to sync configs
- added: option to unban users during sync (this will not sync profile data)
- added: log cleanup settings per sync config
- fixed: ban sync no longer requires sync fields #2
- fixed: improved visibility when manualy running a sync config
- changed: simplified profile data update

0.1 (2014-07-17):

- first release