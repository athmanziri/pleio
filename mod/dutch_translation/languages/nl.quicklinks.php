<?php
$language = array (
  'quicklinks:owner:title' => '%s\'s links',
  'quicklinks:edit:url' => 'Website URL',
  'quicklinks' => 'Mijn links',
  'quicklinks:add' => 'Link toevoegen',
  'quicklinks:move' => 'verplaats',
  'quicklinks:menu:entity:title' => 'Toon in mijn Quicklinks',
  'quicklinks:widget:title' => 'Mijn Quicklinks',
  'quicklinks:widget:description' => 'Toont al je favoriete links',
  'quicklinks:add:title' => 'Link toevoegen',
  'quicklinks:edit:required' => 'Velden met een * zijn verplicht',
  'quicklinks:action:toggle:success:add' => 'Toegevoegd aan mijn Quicklinks',
  'quicklinks:action:toggle:success:remove' => 'Verwijderd van mijn Quicklinks',
  'quicklinks:action:delete:error' => 'Er is een fout opgetreden tijdens het verwijderen van de link',
  'quicklinks:action:delete:success' => 'Link verwijderd',
);
add_translation("nl", $language);
