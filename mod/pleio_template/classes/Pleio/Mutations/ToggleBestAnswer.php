<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class ToggleBestAnswer {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "toggleBestAnswer",
            "inputFields" => [
                "guid" => [ "type" => Type::string() ]
            ],
            "outputFields" => [
                "object" => [
                    "type" => Type::nonNull($registry->get("Event")),
                    "resolve" => function($entity, array $args, $context, ResolveInfo $info) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ],
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $user = elgg_get_logged_in_user_entity();
        if (!$user || !check_entity_relationship($user->guid, "questions_expert", $site->guid)) {
            return [
                "guid" => $question->guid
            ];
        }

        $entity = get_entity($input["guid"]);
        if (!$entity || !in_array($entity->getSubtype(), ["comment", "answer"])) {
            throw new Exception("could_not_save");
        }

        $question = $entity->getContainerEntity();
        if (!$question) {
            throw new Exception("could_not_find");
        }

        if (check_entity_relationship($question->guid, "correctAnswer", $entity->guid)) {
            remove_entity_relationship($question->guid, "correctAnswer", $entity->guid);
        } else {
            $correctAnswers = $question->getEntitiesFromRelationship("correctAnswer", false, 0);
            if ($correctAnswers) {
                foreach ($correctAnswers as $correctAnswer) {
                    remove_entity_relationship($question->guid, "correctAnswer", $correctAnswer->guid);
                }
            }

            add_entity_relationship($question->guid, "correctAnswer", $entity->guid);
        }

        return [
            "guid" => $question->guid
        ];
    }
}
