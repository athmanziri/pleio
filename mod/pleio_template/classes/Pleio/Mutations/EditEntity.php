<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditEntity {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "editEntity",
            "inputFields" => [
                "guid" => [ "type" => Type::nonNull(Type::string()) ],
                "title" => [ "type" => Type::string() ],
                "description" => [ "type" => Type::nonNull(Type::string()) ],
                "richDescription" => [ "type" => Type::string() ],
                "isRecommended" => [ "type" => Type::boolean() ],
                "isFeatured" => [ "type" => Type::boolean() ],
                "featured" => [ "type" => $registry->get("FeaturedInput") ],
                "startDate" => [ "type" => Type::string() ],
                "endDate" => [ "type" => Type::string() ],
                "source" => [ "type" => Type::string() ],
                "location" => [ "type" => Type::string() ],
                "maxAttendees" => [ "type" => Type::string() ],
                "rsvp" => [ "type" => Type::boolean() ],
                "accessId" => [ "type" => Type::int() ],
                "writeAccessId" => [ "type" => Type::int() ],
                "tags" => [ "type" => Type::listOf(Type::string()) ],
                "mentions" => [ "type" => Type::listOf(Type::string())],
            ],
            "outputFields" => [
                "entity" => [
                    "type" => $registry->get("Entity"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!in_array($entity->type, array("group", "object"))) {
            throw new Exception("invalid_object_type");
        }

        switch ($entity->type) {
            case "group":
                $entity->name = $input["name"];
            case "object":
                $entity->title = $input["title"];
            default:
                $entity->description = $input["description"];

                if ($input["richDescription"]) {
                    $entity->richDescription = $input["richDescription"];
                }

                if (isset($input["accessId"])) {
                    $entity->access_id = (int) $input["accessId"];
                }

                if (isset($input["writeAccessId"])) {
                    $entity->write_access_id = (int) $input["writeAccessId"];
                }

                if (elgg_is_admin_logged_in()) {
                    if (isset($input["isRecommended"])) {
                        if ($input["isRecommended"]) {
                            $entity->isRecommended = $input["isRecommended"];
                        } else {
                            unset($entity->isRecommended);
                        }
                    }
                }

                $entity->tags = filter_tags($input["tags"]);

                if (isset($input["mentions"]) && is_array($input["mentions"])) {
                    $entity->mentions = $input["mentions"];
                }
        }

        $result = $entity->save();

        if (in_array($entity->getSubtype(), ["news", "blog", "page", "event"])) {
            if (isset($input["isFeatured"])) {
                $entity->isFeatured = $input["isFeatured"];
            }

            if ($input["featured"]) {
                if (isset($input["featured"]["image"])) {
                    if ($input["featured"]["image"] === "false") {
                        unset($entity->featuredIcontime);
                    } else {
                        Helpers::saveToFeatured($input["featured"]["image"], $entity);
                        $entity->featuredIcontime = time();
                    }
                }

                if ($input["featured"]["video"]) {
                    $entity->featuredVideo = $input["featured"]["video"];
                } else {
                    unset($entity->featuredVideo);
                }

                if ($input["featured"]["positionY"]) {
                    $entity->featuredPositionY = $input["featured"]["positionY"];
                } else {
                    unset($entity->featuredPositionY);
                }
            } else {
                unset($entity->featuredIcontime);
                unset($entity->featuredVideo);
                unset($entity->featuredPositionY);
            }

            if ($input["source"]) {
                $entity->source = $input["source"];
            } else {
                unset($entity->source);
            }

            $result &= $entity->save();
        }

        if ($entity->getSubtype() === "event") {
            $startDate = strtotime($input["startDate"]);
            $endDate = strtotime($input["endDate"]);
            $entity->start_day = $startDate;
            $entity->start_time = $startDate;
            $entity->end_ts = $endDate;

            if ($input["location"]) {
                $entity->location = $input["location"];
            }

            if (isset($input["rsvp"])) {
                $entity->rsvp = $input["rsvp"];
            }

            if (isset($input["maxAttendees"])) {
                $entity->maxAttendees = $input["maxAttendees"];
            }

            $result &= $entity->save();
        }

        if ($entity->getSubtype() === "poll") {
            // @todo
        }

        if ($entity->mentions) {
            Helpers::notifyMentions($entity);
        }

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
