<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class Bookmark {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "bookmark",
            "inputFields" => [
                "guid" => [
                    "type" => Type::nonNull((Type::string())),
                    "description" => "The guid of the entity to bookmark."
                ],
                "isAdding" => [
                    "type" => Type::nonNull(Type::boolean()),
                    "description" => "True when adding, false when removing."
                ]
            ],
            "outputFields" => [
                "object" => [
                    "type" => Type::nonNull($registry->get("Entity")),
                    "resolve" => function($result) {
                        return Resolver::getEntity(null, $result["object"], null);
                    }
                ],
                "isFirstBookmark" => [
                    "type" => Type::nonNull(Type::boolean())
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("not_logged_in");
        }

        $options = [
            "relationship_guid" => $user->guid,
            "relationship" => "bookmarked",
            "count" => true
        ];

        $total = elgg_get_entities_from_relationship($options);
        $is_first_bookmark = ($total === 0) ? true : false;

        if ($input["isAdding"]) {
            $result = add_entity_relationship($user->guid, "bookmarked", $entity->guid);
        } else {
            $result = remove_entity_relationship($user->guid, "bookmarked", $entity->guid);
        }

        if ($result) {
            return [
                "object" => [
                    "guid" => $entity->guid
                ],
                "isFirstBookmark" => $is_first_bookmark
            ];
        }

        throw new Exception("could_not_save");
    }
}
