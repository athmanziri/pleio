<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\EnumType;
use Pleio\TypeRegistry;

class GroupFilter extends EnumType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "GroupFilter",
            "description" => "Show all groups or only mine",
            "values" => [
                "all" => [ "value" => "all" ],
                "mine" => [ "value" => "mine" ]
            ]
        ]);
    }
}
