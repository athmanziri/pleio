<?php
global $MEMCACHE_TIME;
$MEMCACHE_TIME = 0;

/**
 * Memcache wrapper class.
 *
 * @package    Elgg.Core
 * @subpackage Memcache
 */
class ElggMemcache extends ElggSharedMemoryCache {
	/**
	 * Minimum version of memcached needed to run
	 *
	 */
	private static $MINSERVERVERSION = '1.1.12';

	/**
	 * Memcache object
	 */
	private $memcache;

	/**
	 * Expiry of saved items (default timeout after a day to prevent anything getting too stale)
	 */
	private $expires = 86400;

	/**
	 * Namespace variable used to keep various bits of the cache
	 * separate.
	 *
	 * @var string
	 */
	private $namespace;

	/**
	 * The version of memcache running
	 */
	private $version = 0;

	/**
	 * Latest cleartime used to make cache flushable.
	 *
	 * @var int
	 */
	private $latest_clear;

	/**
	 * Connect to memcache.
	 *
	 * @param string $namespace The namespace for this cache to write to -
	 * note, namespaces of the same name are shared!
	 *
	 * @throws ConfigurationException
	 */
	function __construct($namespace = 'default') {
		global $CONFIG;

		$this->setNamespace($namespace);

		// Do we have memcache?
		if (!class_exists('Memcached')) {
			throw new ConfigurationException('PHP memcache module not installed, you must install php7-memcached');
		}

		// Create memcache object
		$this->memcache	= new Memcached;

		// Now add servers
		if (!$CONFIG->memcache_servers) {
			throw new ConfigurationException('No memcache servers defined, please populate the $CONFIG->memcache_servers variable');
		}

		foreach ($CONFIG->memcache_servers as $server) {
			$this->memcache->addServer($server[0], $server[1]);
		}

		// Set some defaults
		if (isset($CONFIG->memcache_expires)) {
			$this->expires = $CONFIG->memcache_expires;
		}

		// Retrieve the latest clear time
		$this->latest_clear = $this->memcache->get($this->namespace);
		if (!$this->latest_clear) {
			$this->clear();
		}
	}

	/**
	 * Set the default expiry.
	 *
	 * @param int $expires The lifetime as a unix timestamp or time from now. Defaults forever.
	 *
	 * @return void
	 */
	public function setDefaultExpiry($expires = 0) {
		$this->expires = $expires;
	}

	/**
	 * Combine a key with the namespace.
	 * Memcache can only accept <250 char key. If the given key is too long it is shortened.
	 *
	 * @param string $key The key
	 *
	 * @return string The new key.
	 */
	private function makeMemcacheKey($key) {
		$prefix = $this->getNamespace() . ":" . $this->latest_clear . ":";

		if (strlen($prefix . $key) > 250) {
			$key = md5($key);
		}

		return $prefix . $key;
	}

	/**
	 * Saves a name and value to the cache
	 *
	 * @param string  $key     Name
	 * @param string  $data    Value
	 * @param integer $expires Expires (in seconds)
	 *
	 * @return bool
	 */
	public function save($key, $data, $expires = null) {
		$key = $this->makeMemcacheKey($key);

		if ($expires === null) {
			$expires = $this->expires;
		}

		$result = $this->memcache->set($key, $data, $expires);
		if ($result === false && function_exists("elgg_log")) {
			elgg_log("MEMCACHE: FAILED TO SAVE $key", 'ERROR');
		}

		return $result;
	}

	/**
	 * Retrieves data.
	 *
	 * @param string $key    Name of data to retrieve
	 * @param int    $offset Offset
	 * @param int    $limit  Limit
	 *
	 * @return mixed
	 */
	public function load($key, $offset = 0, $limit = null) {
		global $MEMCACHE_TIME;

		$key = $this->makeMemcacheKey($key);

		$start_time = microtime(true);

		$result = $this->memcache->get($key);

		$MEMCACHE_TIME += microtime(true) - $start_time;

		if ($result === false && function_exists("elgg_log")) {
			elgg_log("MEMCACHE: FAILED TO LOAD $key", 'ERROR');
		}

		return $result;
	}

	/**
	 * Delete data
	 *
	 * @param string $key Name of data
	 *
	 * @return bool
	 */
	public function delete($key) {
		$key = $this->makeMemcacheKey($key);
		return $this->memcache->delete($key, 0);
	}

	/**
	 * Clears the entire cache?
	 *
	 * @todo write or remove.
	 *
	 * @return true
	 */
	public function clear() {
		$current_time = time();
		$this->memcache->set($this->namespace, $current_time, $this->expires);
		$this->latest_clear = $current_time;
		return true;
	}

	/**
	 * Set the namespace of this cache.
	 * This is useful for cache types (like memcache or static variables) where there is one large
	 * flat area of memory shared across all instances of the cache.
	 *
	 * @param string $namespace Namespace for cache
	 *
	 * @return void
	 */
	public function setNamespace($namespace = "default") {
		global $CONFIG;
		$prefix = "";
		if(!empty($CONFIG->memcache_prefix)) {
			$prefix = $CONFIG->memcache_prefix . ":";
		}
		$this->namespace = $prefix . $namespace;
	}
	/**
	 * Get the namespace currently defined.
	 *
	 * @return string
	 */
	public function getNamespace() {
		return $this->namespace;
	}
}
