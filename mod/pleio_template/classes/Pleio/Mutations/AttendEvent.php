<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AttendEvent {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "attendEvent",
            "inputFields" => [
                "guid" => [ "type" => Type::string() ],
                "state" => [ "type" => Type::string() ]
            ],
            "outputFields" => [
                "entity" => [
                    "type" => $registry->get("Event"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $event = get_entity((int) $input["guid"]);
        if (!$event) {
            throw new Exception("could_not_find");
        }

        if (!$event instanceof \ElggObject || $event->getSubtype() !== "event") {
            throw new Exception("could_not_save");
        }

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("not_logged_in");
        }

        if ($input["state"] === "accept") {
            $options = [
                "relationship_guid" => $event->guid,
                "relationship" => "event_attending",
                "count" => true
            ];
            if ($event->maxAttendees != "" && elgg_get_entities_from_relationship_count($options) >= (int) $event->maxAttendees) {
                throw new Exception("event_is_full");
            } else {
                add_entity_relationship($event->guid, "event_attending", $user->guid);

                $link = Helpers::getURL($event, true);
                $date = Helpers::getEventDateString($event);

                $result = elgg_send_email(
                    $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                    $user->email,
                    "Bevestiging aanmelding \"{$event->title}\"",
                    "Hierbij bevestigen we je aanmelding voor het agenda-item \"{$event->title}\".<br />
                    Locatie: {$event->location}
                    Datum: {$date}<br />
                    Klik op de onderstaande link om het agenda-item te bekijken, je aanmelding te wijzigen en/of de afspraak in je agenda te zetten. <br />
                    <a href=\"{$link}\">$link</a>
                    "
                );
            }
        } else {
            remove_entity_relationship($event->guid, "event_attending", $user->guid);
        }

        if ($input["state"] === "maybe") {
            add_entity_relationship($event->guid, "event_maybe", $user->guid);
        } else {
            remove_entity_relationship($event->guid, "event_maybe", $user->guid);
        }

        if ($input["state"] == "reject") {
            add_entity_relationship($event->guid, "event_reject", $user->guid);
        } else {
            remove_entity_relationship($event->guid, "event_reject", $user->guid);
        }

        return [
            "guid" => $event->guid
        ];
    }
}
