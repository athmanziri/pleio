<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddImage {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "addImage",
            "inputFields" => [
                "image" => [
                    "type" => Type::string()
                ]
            ],
            "outputFields" => [
                "file" => [
                    "type" => $registry->get("FileFolder"),
                    "resolve" => function($file) {
                        return Resolver::getFile(null, $file, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("could_not_save");
        }

        $result = Helpers::saveToImage($input["image"], $user);

        if ($result) {
            return [
                "guid" => $result->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
