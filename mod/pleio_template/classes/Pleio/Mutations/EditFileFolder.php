<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditFileFolder {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "editFileFolder",
            "inputFields" => [
                "guid" => [ "type" => Type::string() ],
                "title" => [ "type" => Type::string() ],
                "file" => [ "type" => Type::string() ],
                "accessId" => [ "type" => Type::int() ],
                "writeAccessId" => [ "type" => Type::int() ],
                "isAccessRecursive" => [ "type" => Type::boolean() ],
            ],
            "outputFields" => [
                "entity" => [
                    "type" => $registry->get("Entity"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!in_array($entity->getSubtype(), array("file", "folder"))) {
            throw new Exception("invalid_object_subtype");
        }

        if ($input["title"]) {
            $entity->title = $input["title"];
        }

        if (isset($input["accessId"])) {
            $entity->access_id = $input["accessId"];
        } else {
            $entity->access_id = get_default_access();
        }

        if (isset($input["writeAccessId"])) {
            $entity->write_access_id = $input["writeAccessId"];
        } else {
            $entity->write_access_id = ACCESS_PRIVATE;
        }

        if (isset($input["isAccessRecursive"]) && $entity->getSubtype() == "folder" && $input["isAccessRecursive"] == true) {
            Helpers::setRecursiveFileAccess($entity);
        }

        if ($entity->getSubtype() === "file" && $input["file"]) {
            $file = Helpers::getFile($input["file"]);

            if (!$file) {
                throw new Exception("no_file");
            }

            if ($file["error"] !== 0) {
                throw new Exception("invalid_filesize");
            }

            move_uploaded_file($file["tmp_name"], $entity->getFilenameOnFilestore());
            $entity->originalfilename = $file["name"];
            $mime_type = \ElggFile::detectMimeType($entity->getFilenameOnFilestore(), $file["type"]);
            $entity->setMimeType($mime_type);
            $entity->simpletype = file_get_simple_type($mime_type);
        }

        $result = $entity->save();

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
