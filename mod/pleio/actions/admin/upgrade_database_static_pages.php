<?php
if(elgg_is_active_plugin("pleio_template")) {

    function inGroup($entity, $cnt=0) {
        $cnt++;
        if ($cnt > 10) {
            return NULL;
        }

        $parent = $entity->getContainerEntity();
        if ($parent instanceof ElggGroup) {
            return $parent;
        } elseif($parent instanceof ElggSite) {
            return false;
        } else {
            return inGroup($parent, $cnt);
        }
    }

    function get_or_create_base_wiki($group) {
        $options = [
            "type" => "object",
            "subtypes" => ["wiki", ],
            "container_guids" => [$group->guid],
            "limit" => false
        ];

        $wikis = elgg_get_entities($options);
        foreach (elgg_get_entities($options) as $wiki) {
            if ($wiki->title == "Statische Wiki's") {
                return $wiki;
            }
        }

        $wiki = new ElggObject();
        $wiki->subtype = "wiki";
        $wiki->title = "Statische Wiki's";
        $wiki->description = "";
        $wiki->owner_guid = $group->owner_guid;
        $wiki->container_guid = $group->guid;
        $wiki->access_id = 1;
        $wiki->save();
        return $wiki;
    }

    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    $site = elgg_get_site_entity();
    $site_guid = (int) $site->guid;

    $subtype_wiki_id = add_subtype("object", "wiki");
    $subtype_page_id = add_subtype("object", "page");

    $options = [
        "type" => "object",
        "subtypes" => ["static", "static_top"],
        "limit" => false
    ];

    foreach (elgg_get_entities($options) as $page) {
        $group = inGroup($page);

        if ($group === false) { // on site level: convert to page
            update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_page_id} WHERE guid = {$page->guid}");
            _elgg_invalidate_memcache_for_entity($page->guid);

        } else if ($group === NULL) { // lost: put on site level and convert to page

            update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_page_id}, container_guid = {$site_guid} WHERE guid = {$page->guid}");
            _elgg_invalidate_memcache_for_entity($page->guid);

        } else { // in group: convert to wiki

            // if parent == group get or create base wiki
            $parent = $page->getContainerEntity();
            if ($parent instanceof ElggGroup) {
                // get or create base Wiki
                $base_wiki = get_or_create_base_wiki($parent);
                $container_guid = $base_wiki->guid;
            } else {
                $container_guid = $page->container_guid;
            }

            update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_wiki_id}, container_guid = {$container_guid} WHERE guid = {$page->guid}");
            _elgg_invalidate_memcache_for_entity($page->guid);
        }
    }

    elgg_set_ignore_access($ia);

    system_message("Statische pagina's geconverteerd voor nieuwe template");

    forward(REFERER);
}
