<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditGroup {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "editGroup",
            "inputFields" => [
                "guid" => [ "type" => Type::string() ],
                "name" => [ "type" => Type::string() ],
                "icon" => [ "type" => Type::string() ],
                "featured" => [ "type" => $registry->get("FeaturedInput") ],
                "isClosed" => [ "type" => Type::boolean(), "description" => "True when membership has to be requested by the user, False when every user can join the group." ],
                "isFeatured" => [ "type" => Type::boolean() ],
                "autoNotification" => [ "type" => Type::boolean() ],
                "description" => [ "type" => Type::string() ],
                "richDescription" => [ "type" => Type::string() ],
                "introduction" => [ "type" => Type::string() ],
                "welcomeMessage" => [ "type" => Type::string() ],
                "tags" => [ "type" => Type::listOf(Type::string()) ],
                "plugins" => [ "type" => Type::listOf($registry->get("Plugins")) ]
            ],
            "outputFields" => [
                "group" => [
                    "type" => $registry->get("Group"),
                    "resolve" => function($group) {
                        return Resolver::getEntity(null, $group, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $group = get_entity((int) $input["guid"]);
        if (!$group) {
            throw new Exception("could_not_find");
        }

        if (!$group->canEdit() || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_save");
        }

        if ($input["icon"]) {
            Helpers::saveToIcon($input["icon"], $group);
            $group->icontime = time();
        } else {
            unset($group->icontime);
        }



        $group->name = $input["name"];
        $group->membership = $input["isClosed"] ? ACCESS_PRIVATE : ACCESS_PUBLIC;
        $group->access_id = ACCESS_PUBLIC;
        $group->description = $input["description"];
        $group->richDescription = $input["richDescription"];
        $group->introduction = $input["introduction"];
        $group->tags = filter_tags($input["tags"]);
        $group->plugins = array_unique($input["plugins"]);

        if (elgg_is_admin_logged_in() && isset($input["isFeatured"])) {
            $group->isFeatured = $input["isFeatured"];
        }

        if (isset($input["autoNotification"])) {
            if ($input["autoNotification"]) {
                $group->autoNotification = $input["autoNotification"];
            } else {
                unset($group->autoNotification);
            }
        }

        if ($input["featured"]) {
            if ($input["featured"]["image"]) {
                if ($input["featured"]["image"] === "false") {
                    unset($group->featuredIcontime);
                } else {
                    Helpers::saveToFeatured($input["featured"]["image"], $group);
                    $group->featuredIcontime = time();
                }
            }

            if ($input["featured"]["video"]) {
                $group->featuredVideo = $input["featured"]["video"];
            } else {
                unset($group->featuredVideo);
            }

            if ($input["featured"]["positionY"]) {
                $group->featuredPositionY = $input["featured"]["positionY"];
            } else {
                unset($group->featuredPositionY);
            }
        } else {
            unset($group->featuredIcontime);
            unset($group->featuredVideo);
            unset($group->featuredPositionY);
        }

        if (!empty(trim(strip_tags($input["welcomeMessage"])))) {
            $group->setPrivateSetting("group_tools:welcome_message", $input["welcomeMessage"]);
        } else {
            $group->removePrivateSetting("group_tools:welcome_message");
        }

        $result = $group->save();

        if ($result) {
            return [
                "guid" => $group->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
