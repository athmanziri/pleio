<?php
if(elgg_is_active_plugin("pleio_template")) {

    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    $subtype_id = add_subtype("object", "file");

    $options = [
        "type" => "object",
        "subtypes" => ["image"],
        "limit" => false
    ];

    foreach (elgg_get_entities($options) as $image) {
        $album = get_entity($image->container_guid);
        update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_id} WHERE guid = {$image->guid}");
        add_entity_relationship($image->container_guid, "folder_of", $image->guid);
        update_data("UPDATE {$dbprefix}entities SET container_guid = {$album->container_guid} WHERE guid = {$image->guid}");
        _elgg_invalidate_memcache_for_entity($image->guid);
    }

    # repair photo albums
    function get_or_create_folder($folders, $album) {
        foreach ($folders as $folder) {
            if ($folder->title == "Fotoalbums") {
                return $folder;
            }
        }
        $folder = new ElggObject();
        $folder->subtype = "folder";
        $folder->title = "Fotoalbums";
        $folder->description = "";
        $folder->owner_guid = $album->owner_guid;
        $folder->container_guid = $album->container_guid;
        $folder->access_id = 1;
        $folder->save();
        return $folder;
    }

    $dbprefix = elgg_get_config("dbprefix");
    $subtype_id = add_subtype("object", "folder");

    $options = [
        "type" => "object",
        "subtypes" => ["album"],
        "limit" => false
    ];

    foreach (elgg_get_entities($options) as $album) {
        $folder_options = [
            "type" => "object",
            "subtypes" => ["folder"],
            "limit" => false,
            "container_guids" => [$album->container_guid]
        ];
        $folder= get_or_create_folder(elgg_get_entities($folder_options), $album);
        update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_id} WHERE guid = {$album->guid}");
        create_metadata($folder->guid, 'parent_guid', 0, 'integer', $folder->owner_guid, 1);
        create_metadata($folder->guid, 'write_access_id', 1, 'integer', $folder->owner_guid, 1);
        create_metadata($album->guid, 'parent_guid', $folder->guid, 'integer', $album->owner_guid, 1);
        _elgg_invalidate_memcache_for_entity($album->guid);
    }

    elgg_set_ignore_access($ia);

    system_message("Fotoalbums geconverteerd voor nieuwe template");

    forward(REFERER);
}
