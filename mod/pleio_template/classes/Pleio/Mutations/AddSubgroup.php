<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddSubgroup {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "addSubgroup",
            "inputFields" => [
                "name" => [ "type" => Type::string() ],
                "groupGuid" => [ "type" => Type::int() ],
                "members" => [ "type" => Type::listOf(Type::int()) ]
            ],
            "outputFields" => [
                "success" => [ "type" => Type::boolean() ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $group = get_entity($input["groupGuid"]);
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        $id = create_access_collection($input["name"], $group->guid);

        if ($group->subpermissions) {
            $subpermissions = unserialize($group->subpermissions);
        }

        if (!is_array($subpermissions)) {
            $subpermissions = array();
        }

        array_push($subpermissions, $id);

        $group->subpermissions = serialize($subpermissions);
        $group->save();

        update_access_collection($id, $input["members"]);

        return [
            "success" => true
        ];
    }
}
