<?php

echo "<div>" . elgg_echo("pleio:invite:description") . "</div>";

$form_vars = array(
    "class" => "elgg-form-settings",
    "enctype" => "multipart/form-data"
);
$body_vars = array();

echo elgg_view_form("admin/users/invite", $form_vars, $body_vars);