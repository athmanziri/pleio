<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddGroupWidget {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "addGroupWidget",
            "inputFields" => [
                "groupGuid" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "position" => [
                    "type" => Type::nonNull(Type::int())
                ],
                "type" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "settings" => [
                    "type" => Type::listOf($registry->get("WidgetSettingInput"))
                ]
            ],
            "outputFields" => [
                "entity" => [
                    "type" => $registry->get("Widget"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $group = get_entity((int) $input["groupGuid"]);
        if (!$group || $group->type !== "group") {
            throw new Exception("could_not_find");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        $entity = new \ElggObject();
        $entity->subtype = "page_widget";
        $entity->container_guid = $group->guid;
        $entity->position = $input["position"];
        $entity->access_id = $group->access_id;
        $entity->widget_type = $input["type"];

        $result = $entity->save();

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
