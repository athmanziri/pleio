#!/bin/sh

# Configure default environment variables
export SMTP_HOST=${SMTP_HOST:-172.17.0.1}
export SMTP_HOSTNAME=${SMTP_HOST:-mail.pleio.nl}

envsubst < /app/docker/ssmtp.conf > /etc/ssmtp/ssmtp.conf
