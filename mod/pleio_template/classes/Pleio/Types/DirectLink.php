<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class DirectLink extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "DirectLink",
            "fields" => [
                "title" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "link" => [
                    "type" => Type::nonNull(Type::string())
                ]
            ]
        ]);
    }
}
