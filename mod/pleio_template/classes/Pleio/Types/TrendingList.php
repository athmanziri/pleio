<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class TrendingList extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "TrendingList",
            "fields" => [
                "tag" => [ "type" => Type::string() ],
                "likes" => [ "type" => Type::int() ]
            ]
        ]);
    }
}
