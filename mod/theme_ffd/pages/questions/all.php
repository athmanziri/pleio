<?php
/**
 * All questions page
 *
 * @package ElggQuestions
 */

elgg_set_page_owner_guid(elgg_get_logged_in_user_guid());
elgg_register_title_button();

elgg_push_breadcrumb(elgg_echo('questions'));

$options = [
    'type' => 'object',
    'subtype' => 'question',
    'full_view' => false,
    'list_type_toggle' => false,
];

$order_by = get_input("order_by");
if ($order_by === "latest_question") {
    $options["order_by"] = "e.time_created DESC";
} else {
    $options["order_by"] = "e.last_action DESC";
}

$content = elgg_list_entities($options);

if (!$content) {
    $content = elgg_echo('questions:none');
}

$title = elgg_echo('questions:everyone');

$body = elgg_view_layout('content', array(
    'title' => $title,
    'content' => $content,
));

echo elgg_view_page($title, $body);
