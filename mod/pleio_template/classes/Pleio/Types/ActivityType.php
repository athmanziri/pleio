<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class ActivityType extends EnumType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "ActivityType",
            "description" => "The type of activity",
            "values" => [
                "create" => [ "value" => "create" ],
                "update" => [ "value" => "update" ]
            ]
        ]);
    }
}
