<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class VoteOnPoll {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "voteOnPoll",
            "inputFields" => [
                "guid" => [ "type" => Type::nonNull(Type::string()) ],
                "response" => [ "type" => Type::nonNull(Type::string()) ],
            ],
            "outputFields" => [
                "entity" => [
                    "type" => Type::nonNull($registry->get("Entity")),
                    "resolve" => function($entity, array $args, $context, ResolveInfo $info) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $input["guid"] = (int) $input["guid"];

        $entity = get_entity($input["guid"]);
        if (!$entity || $entity->getSubtype() !== "poll") {
            throw new Exception("could_not_find");
        }

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("not_logged_in");
        }

        if (!$input["response"]) {
            throw new Exception("invalid_answer");
        }

        $options = [
            "annotation_name" => "vote",
            "annotation_owner_guid" => $user->guid,
            "guid" => $entity->guid,
            "limit" => 1
        ];

        if (elgg_get_annotations($options)) {
            throw new Exception("already_voted");
        }

        $entity->annotate("vote", $input["response"], $entity->access_id);

        return [
            "guid" => $entity->guid
        ];
    }
}
