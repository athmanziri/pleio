<?php
$dutch = array(
	'reportedcontent:usersettings:notify:description' => "I wish to be notified when somebody submits a report about inappropriate content",
	'reportedcontent:usersettings:notify:subject' => "A new item was reported",
	'reportedcontent:usersettings:notify:message' => "Hi,

%s reported a new item. To view the report check the link below.
%s"
);

add_translation("nl", $dutch);