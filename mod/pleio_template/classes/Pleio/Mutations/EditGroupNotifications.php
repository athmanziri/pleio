<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditGroupNotifications {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "editGroupNotifications",
            "inputFields" => [
                "guid" => [
                    "type" => Type::string()
                ],
                "getsNotifications" => [
                    "type" => Type::boolean()
                ]
            ],
            "outputFields" => [
                "group" => [
                    "type" => $registry->get("Group"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("not_logged_in");
        }

        $group = get_entity(((int) $input["guid"]));
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find");
        }

        if (!$group->isMember($user)) {
            throw new Exception("user_not_member_of_group");
        }

        $getsNotifications = $input["getsNotifications"];
        if ($getsNotifications) {
            add_entity_relationship($user->guid, "subscribed", $group->guid);
        } else {
            remove_entity_relationship($user->guid, "subscribed", $group->guid);
        }

        return  [
            "guid" => $group->guid
        ];
    }
}
