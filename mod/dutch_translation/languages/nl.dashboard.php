<?php
$language = array (
  'dashboard:widget:group:title' => 'Activiteiten in deze groep',
  'dashboard:widget:group:desc' => 'Bekijk de activiteit van een van je groepen',
  'dashboard:widget:group:select' => 'Selecteer een groep',
  'dashboard:widget:group:noactivity' => 'Er zijn geen activiteiten beschikbaar',
  'dashboard:widget:group:noselect' => 'Bewerk deze widget om een groep te selecteren',
);
add_translation("nl", $language);
