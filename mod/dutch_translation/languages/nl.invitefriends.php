<?php
$language = array (
  'friends:invite' => 'Iemand uitnodigen',
  'invitefriends:introduction' => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">







Als je iemand wilt uitnodigen om gebruiker te worden van deze site, vul dan hieronder de e-mailadressen in (<strong>Let op één per regel</strong>):',
  'invitefriends:message' => 'Voeg een uitnodigende tekst toe:',
  'invitefriends:subject' => 'Uitnodiging om gebruiker te worden van %s',
  'invitefriends:success' => 'De uitnodigingen zijn verstuurd..',
  'invitefriends:invitations_sent' => 'Uitnodigingen verstuurd:%s. De volgende problemen deden zich voor:',
  'invitefriends:email_error' => 'De uitnodigingen zijn verstuurd, maar de volgende adressen bleken ongeldig: %s',
  'invitefriends:already_members' => 'Onderstaande zijn al gebruiker: %s',
  'invitefriends:noemails' => 'Er is geen e-mailadres ingevuld.',
  'invitefriends:message:default' => 'Hallo,

Ik wil je uitnodigen om gebruiker te worden van %s.',
  'invitefriends:email' => 'Je bent uitgenodigd om gebruiker te worden van %s door %s. 

Lees hieronder de uitnodigingstekst:
%s 

Om gebruiker te worden, klik op de onderstaande link: %s',
  'invitefriends:registration_disabled' => 'Registratie van nieuwe gebruikers is uitgeschakeld op deze site, je kunt geen nieuwe gebruikers uitnodigen.',
);
add_translation("nl", $language);
