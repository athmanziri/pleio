<?php
$language = array (
  'item:object:ws_pack_application' => 'Webservice applicatie',
  'item:object:ws_pack_application_user_setting' => 'Webservice applicatie gebruikers instellingen',
  'ws_pack:deactivate' => 'Deactiveren',
  'ws_pack:activate' => 'Activeren',
  'admin:administer_utilities:ws_pack' => 'Webservice applicaties',
  'ws_pack:menu:admin:applications:active' => 'Actief',
  'ws_pack:menu:admin:applications:pending' => 'In afwachting van goedkeuring',
  'ws_pack:menu:admin:applications:inactive' => 'inactief',
  'ws_pack:menu:admin:applications:disabled' => 'Uitgeschakeld',
  'ws_pack:admin:listing:states:legend' => '<b> Status legend: </ b> 
Actief: Deze toepassingen worden toegestaan ​​om de API te gebruiken. 
In afwachting: Deze toepassingen hebben toegang tot de API gevraagd, moet u goed-of afkeuren deze aanvraag. 
Inactief: Deze toepassingen zijn (temporaraly) uitgeschakeld. 
Uitgeschakeld: Deze toepassingen zijn de toegang tot de API ontzegd.</b>',
  'ws_pack:api:application:api_keys:key' => 'API key',
  'ws_pack:api:application:api_keys:secret' => 'Geheim',
);
add_translation("nl", $language);
