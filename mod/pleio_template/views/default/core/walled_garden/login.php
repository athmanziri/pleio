<?php
$site = elgg_get_site_entity();

$description = elgg_get_plugin_setting("walled_garden_description", "pleio");
if (!$description) {
    $description = "<p>" . elgg_echo("pleio:walled_garden_description") . "</p>";
}

$login_box = elgg_view('core/account/login_box', array('module' => 'walledgarden-login'));
?>
	<div class="walled-garden_titles">
		<h1 class="walled-garden_title">
			<?php echo $site->name; ?>
		</h1>
		<h2 class="walled-garden_subtitle">
			<?php echo $site->description; ?>
		</h2>
	</div>
	<div class="content">
		<?php echo $description; ?>
	</div>
	<?php echo $login_box; ?>