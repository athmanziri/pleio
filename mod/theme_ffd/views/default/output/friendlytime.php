<?php
/**
 * Friendly time
 * Translates an epoch time into a human-readable time.
 * 
 * @uses string $vars['time'] Unix-style epoch timestamp
 */


$timestamp = htmlspecialchars(strftime("%d %B %Y, %H:%M", $vars['time']));
$friendly_time = $timestamp;

echo "<acronym title=\"$timestamp\">$friendly_time</acronym>";
