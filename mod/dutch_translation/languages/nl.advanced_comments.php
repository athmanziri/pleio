<?php
$language = array (
  'advanced_comments' => 'Geavanceerde reacties',
  'advanced_comments:header:order' => 'Volgorde van reacties',
  'advanced_comments:header:order:asc' => 'Oudste eerst',
  'advanced_comments:header:order:desc' => 'Nieuwste eerst',
  'advanced_comments:header:limit' => 'Aantal',
  'advanced_comments:header:auto_load' => 'Automatisch laden',
  'advanced_comments:comment:logged_out' => 'Reageren is alleen mogelijk voor aangemelde gebruikers',
  'advanced_comments:settings:show_login_form' => 'Toon aanmeldformulier for afgemelde gebruikers onder de commentaren',
);
add_translation("nl", $language);
