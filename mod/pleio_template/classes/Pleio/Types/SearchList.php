<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class SearchList extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "SearchList",
            "fields" => [
                "total" => [
                    "type" => Type::nonNull(Type::int())
                ],
                "totals" => [
                    "type" => Type::listOf($registry->get("SearchTotal"))
                ],
                "edges" => [
                    "type" => Type::listOf($registry->get("Entity"))
                ]
            ]
        ]);
    }
}
