<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditEmail {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "editEmail",
            "inputFields" => [
                "guid" => [
                    "type" => Type::string()
                ],
                "email" => [
                    "type" => Type::string()
                ]
            ],
            "outputFields" => [
                "user" => [
                    "type" => $registry->get("User"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $entity = get_entity(((int) $input["guid"]));
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!$entity instanceof \ElggUser) {
            throw new Exception("not_a_user");
        }

        if (elgg_is_active_plugin("pleio")) {
            $profile_handler = new \ModPleio\ProfileHandler($entity);

            try {
                $body = $profile_handler->changeEmail($input["email"])->getBody();
                $response = json_decode($body);
            } catch (Exception $e) {
                throw new Exception("could_not_save");
            }

            if (!$response->success) {
                throw new Exception($response->message);
            }
        } else {
            $email = trim($input["email"]);
            if (!is_email_address($email)) {
                throw new Exception("invalid_email");
            }

            if (get_user_by_email($email)) {
                throw new Exception("email_already_in_use");
            }

            set_input("guid", $entity->guid);
            set_input("email", $email);
            security_tools_prepare_email_change();

            return [
                "guid" => $entity->guid
            ];

            throw new Exception("could_not_save");
        }
    }
}
