<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class LeaveGroup {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "leaveGroup",
            "description" => "Leave a group.",
            "inputFields" => [
                "guid" => [
                    "type" => Type::string(),
                    "description" => "The guid of the group to leave."
                ],
            ],
            "outputFields" => [
                "group" => [
                    "type" => $registry->get("Group"),
                    "resolve" => function($group) {
                        return Resolver::getEntity(null, $group, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $group = get_entity((int) $input["guid"]);
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find");
        }

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("not_logged_in");
        }

        if ($group->owner_guid == $user->guid) {
            throw new Exception("could_not_leave");
        }

        if (!$group->leave($user)) {
            throw new Exception("could_not_leave");
        }

        if ($group->group_acl) {
            remove_user_from_access_collection($user->guid, $group->group_acl);
        }

        remove_entity_relationship($user->guid, "membership_request", $group->guid);
        remove_entity_relationship($user->guid, "invited", $group->guid);

        return [
            "guid" => $group->guid
        ];
    }
}
