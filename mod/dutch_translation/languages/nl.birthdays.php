<?php
$language = array (
  'birthdays:settings:field_selection' => 'Verjaardagsveld',
  'birthdays:settings:field_selection:description' => 'Selecteer het profielveld welke de verjaardag bevat van je gebruikers.',
  'birthdays:settings:field_selection:none' => 'Geen',
  'birthdays:settings:other' => 'Andere instellingen',
  'birthdays:settings:interval' => 'Toon verjaardagen  binnen',
  'birthdays:settings:interval:days' => 'dagen',
  'birthdays:profile_field:type' => 'Verjaardag',
  'birthdays:widget:title' => 'Verjaardagen',
  'birthdays:widget:description' => 'Toon de aankomende verjaardagen.',
  'birthdays:widget:none' => 'Geen aankomende verjaardagen.',
  'birthdays:widget:not_configured' => 'De verjaardagen plugin is niet ingesteld. Vraag de sitebeheerder om het verjaardagenveld te configureren.',
);
add_translation("nl", $language);
