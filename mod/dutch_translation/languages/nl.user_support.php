<?php
$language = array (
  'user_support:support_type:status:open' => 'Open',
  'user_support:support_type:status:closed' => 'Gesloten',
  'user_support:comment_close' => 'Reageer en sluit',
  'user_support:staff_gatekeeper' => 'Deze pagina is alleen toegankelijk voor support medewerkers',
  'user_support:settings:support_tickets:title' => 'Support Tickets instellingen',
  'user_support:settings:support_tickets:help_group' => 'Selecteer een groep waar gebruikers vragen kunnen stellen',
  'user_support:settings:support_tickets:help_group:none' => 'Geen support groep',
  'user_support:settings:help:title' => 'Contextuele help instellingen',
  'user_support:settings:help:enabled' => 'Contextuele help inschakelen?',
  'user_support:settings:help_center:title' => 'Help Center instellingen',
  'user_support:settings:help_center:add_help_center_site_menu_item' => 'Voeg een site menu item toe voor het Help Center',
  'user_support:settings:help_center:show_floating_button' => 'Toon een snelle toegang knop naar het Help Center',
  'user_support:settings:help_center:show_floating_button:left_top' => 'Links - Boven',
  'user_support:settings:help_center:show_floating_button:left_bottom' => 'Links - Onder',
  'user_support:settings:help_center:show_floating_button:right_top' => 'Rechts - Boven',
  'user_support:settings:help_center:show_floating_button:right_bottom' => 'Rechts - Onder',
  'user_support:settings:help_center:float_button_offset' => 'Verticale offset van de knop',
  'user_support:settings:help_center:show_as_popup' => 'Toon het Help Center in een popup',
  'user_support:settings:faq:title' => 'FAQ instellingen',
  'user_support:settings:faq:add_faq_site_menu_item' => 'Voeg een site menu items toe voor de FAQ',
  'user_support:settings:faq:add_faq_footer_menu_item' => 'Voeg een footer menu item toe voor de FAQ',
  'user_support:settings:other:title' => 'Overige',
  'user_support:settings:other:ignore_site_guid' => 'Negeer site_guid bij het ophalen van helpteksten en FAQ\'s. Kan gebruikt worden op multisite installaties om help en FAQ te delen tussen sites.',
  'river:create:object:support_ticket' => '%s plaatste commentaar bij %s',
  'user_support:support_ticket:promote' => 'Maak FAQ',
  'user_support:menu:faq:group' => 'Groep FAQ',
  'user_support:menu_user_hover:make_staff' => 'Voeg toe als support medewerker',
  'user_support:menu_user_hover:remove_staff' => 'Verwijder als support medewerker',
  'user_support:faq:create:title' => 'Maak een FAQ item',
  'user_support:tickets:owner:title' => '%s Support Tickets',
  'user_support:tickets:owner:archive:title' => '%s gesloten Support Tickets',
  'user_support:group:tool_option' => 'Schakel FAQ ondersteuning voor groepen in',
  'user_support:faq:group:title' => '%s FAQ',
  'user_support:widgets:faq:title' => 'FAQ',
  'user_support:widgets:faq:description' => 'Toon een lijst met de meest recent toegevoegde FAQ items',
  'user_support:widgets:support_ticket:title' => 'Support tickets',
  'user_support:widgets:support_ticket:description' => 'Toon een lijst van jouw Support tickets',
  'user_support:widgets:support_ticket:filter' => 'Welke tickets wil je zien',
  'user_support:widgets:support_ticket:filter:all' => 'Alle',
  'user_support:widgets:support_staff:title' => 'Support afdeling',
  'user_support:widgets:support_staff:description' => 'Toon een lijst met de open Support tickets',
  'user_support:action:support_staff:added' => 'De gebruiker is toegevoegd aan de support medewerkers',
  'user_support:action:support_staff:removed' => 'De gebruiker is geen support medewerker meer',
  'user_support:help_center:help:title' => 'Contextuele help',
  'user_support' => 'Helpdesk',
  'user_support:usersettings:admin_notify' => 'Wil je een bericht ontvangen als een Helpdesk ticket wordt aangemaakt/bijgewerkt',
  'user_support:notify:admin:create:subject' => 'Een nieuwe Helpdesk ticket is gemeld',
  'user_support:notify:admin:create:message' => 'Hallo,

%s heeft een nieuwe Helpdesk ticket gemeld:
%s

Om de ticket te bekijken klik op de volgende link:
%s',
  'user_support:notify:admin:updated:subject' => 'Een Helpdesk ticket is bijgewerkt',
  'user_support:notify:admin:updated:message' => 'Hallo,

%s heeft de Helpdesk ticket %s bijgewerkt:
%s

Om de ticket te bekijken klik op de volgende link:
%s',
  'user_support:help_center:help_group' => 'Ga naar de groep over de helpdesk',
  'item:object:faq' => 'Veelgestelde vragen (FAQ)',
  'item:object:help' => 'Contextuele help',
  'item:object:support_ticket' => 'Hulpvraag',
  'user_support:support_type' => 'Wil je een vraag stellen, een fout melden of een functionele wens doorgeven?',
  'user_support:support_type:question' => 'Een vraag',
  'user_support:support_type:bug' => 'Een foutmelding',
  'user_support:support_type:request' => 'Een functionele wens',
  'user_support:anwser' => 'Antwoord',
  'user_support:anwser:short' => 'A',
  'user_support:question' => 'Tekst',
  'user_support:question:short' => 'V',
  'user_support:url' => 'Internetadres',
  'user_support:allow_comments' => 'Reacties toestaan',
  'user_support:read_more' => 'Lees verder',
  'user_support:help_context' => 'Contextuele help',
  'user_support:reopen' => 'Heropenen',
  'user_support:last_comment' => 'laatste reactie door: %s',
  'river:comment:object:support_ticket' => '%s plaatste een commentaar op %s',
  'user_support:support_ticket:closed' => 'Je hulpvraag is afgesloten',
  'user_support:support_ticket:reopened' => 'Je hulpvraag is weer geopend',
  'user_support:menu:support_tickets' => 'Hulpvragen',
  'user_support:menu:support_tickets:archive' => 'Archief van hulpvragen',
  'user_support:menu:support_tickets:mine' => 'Mijn hulpvragen',
  'user_support:menu:support_tickets:mine:archive' => 'Mijn afgesloten hulpvragen',
  'user_support:menu:faq' => 'Veelgestelde vragen (FAQ)',
  'user_support:menu:faq:create' => 'Maak een FAQ',
  'user_support:button:text' => 'Helpdesk',
  'user_support:button:hover' => 'Open de helpdesk',
  'user_support:help_center:title' => 'Helpdesk',
  'user_support:help_center:ask' => 'Stel een vraag of meld een fout',
  'user_support:help_center:help' => 'Maak een helptekst bij deze pagina',
  'user_support:help_center:faq:title' => 'Veelgestelde vragen (FAQ)',
  'user_support:forms:help:title' => 'Maak een helptekst bij deze pagina',
  'user_support:faq:edit:title:edit' => 'Een helptekst (FAQ) bewerken',
  'user_support:tickets:list:title' => 'Hulpvragen',
  'user_support:tickets:mine:title' => 'Mijn hulpvragen',
  'user_support:tickets:mine:archive:title' => 'Mijn afgesloten hulpvragen',
  'user_support:tickets:archive:title' => 'Archief van hulpvragen',
  'user_support:faq:list:title' => 'Alle veelgestelde vragen',
  'user_support:faq:not_found' => 'Geen helpteksten (FAQ) beschikbaar',
  'user_support:action:help:edit:error:input' => 'Er ging iets mis bij het maken / bewerken van de helptekst. Excuses voor het ongemak. Kun je het nog een keer proberen?',
  'user_support:action:help:edit:error:save' => 'Er ging iets mis bij het opslaan van de helptekst. Excuses voor het ongemak. Kun je het nog een keer proberen?',
  'user_support:action:help:edit:success' => 'De helptekst is opgeslagen',
  'user_support:action:help:delete:error:delete' => 'Er ging iets mis bij het verwijderen van de helptekst. Excuses voor het ongemak. Kun je het nog een keer proberen?',
  'user_support:action:help:delete:success' => 'De helptekst is verwijderd',
  'user_support:action:ticket:edit:error:input' => 'Er ging iets mis bij het maken / bewerken van de hulpvraag. Excuses voor het ongemak. Kun je het nog een keer proberen?',
  'user_support:action:ticket:edit:error:save' => 'Er ging iets mis bij het opslaan van de hulpvraag. Excuses voor het ongemak. Kun je het nog een keer proberen?',
  'user_support:action:ticket:edit:success' => 'De hulpvraag is verzonden',
  'user_support:action:ticket:delete:error:delete' => 'Er ging iets mis bij het verwijderen van de hulpvraag. Excuses voor het ongemak. Kun je het nog een keer proberen?',
  'user_support:action:ticket:delete:success' => 'De hulpvraag is verwijderd',
  'user_support:action:faq:delete:error:delete' => 'Er ging iets mis bij het verwijderen van de helptekst (FAQ). Excuses voor het ongemak. Kun je het nog een keer proberen?',
  'user_support:action:faq:delete:success' => 'De helptekst (FAQ) is verwijderd',
  'user_support:action:faq:edit:error:input' => 'Er ging iets mis bij het maken / bewerken van de helptekst. Voer een vraag en een antwoord in!',
  'user_support:action:faq:edit:error:create' => 'Er ging iets mis bij het verwijderen van de helptekst (FAQ). Excuses voor het ongemak. Kun je het nog een keer proberen?',
  'user_support:action:faq:edit:success' => 'De helptekst (FAQ) is opgeslagen',
  'user_support:action:ticket:close:error:disable' => 'Er ging iets mis bij het afsluiten van de hulpvraag. Excuses voor het ongemak. Kun je het nog een keer proberen?',
  'user_support:action:ticket:close:success' => 'Deze hulpvraag is afgesloten',
  'user_support:action:ticket:reopen:error:enable' => 'Er ging iets mis bij het heropenen van de hulpvraag. Excuses voor het ongemak. Kun je het nog een keer proberen?',
  'user_support:action:ticket:reopen:success' => 'De hulpvraag is heropend',
);
add_translation("nl", $language);
