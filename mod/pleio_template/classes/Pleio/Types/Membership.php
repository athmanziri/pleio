<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class Membership extends EnumType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "description" => "The type of membership.",
            "values" => [
                "not_joined" => [ "value" => "not_joined" ],
                "requested" => [ "value" => "requested" ],
                "invited" => [ "value" => "invited" ],
                "joined" => [ "value" => "joined" ],
            ]
        ]);
    }
}
