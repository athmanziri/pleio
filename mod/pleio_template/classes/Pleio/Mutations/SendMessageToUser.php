<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class SendMessageToUser {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "sendMessageToUser",
            "description" => "Send a message to a user.",
            "inputFields" => [
                "guid" => [
                    "type" => Type::string(),
                    "description" => "The guid of the user to send the message to."
                ],
                "subject" => [
                    "type" => Type::string(),
                    "description" => "The subject of the message."
                ],
                "message" => [
                    "type" => Type::string(),
                    "description" => "The message to send."
                ]
            ],
            "outputFields" => [
                "success" => [ "type" => Type::boolean() ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        set_time_limit(0);

        $site = elgg_get_site_entity();
        $current_user = elgg_get_logged_in_user_entity();

        $user = get_entity((int) $input["guid"]);

        if (!$user) {
            throw new Exception("could_not_find");
        }

        if (!$user instanceof \ElggUser) {
            throw new Exception("could_not_send");
        }

        $result = elgg_send_email(
            $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
            $user->email,
            $input['subject'],
            $input['message'],
            array("reply_to" => $current_user->email)
        );

        return [
            "success" => $result
        ];
    }
}
