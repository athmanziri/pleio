<?php
$language = array (
  'Profile:werklokatie' => 'werklocatie',
  'Profile:name' => 'werklocatie',
  'Profile:Werklokatie' => 'werklocatie',
  'Profile:Werklokate' => 'werklocatie',
  'Profile:Ambtenaar' => 'ambtenaar',
  'profile:Werklocatie' => 'Werklocatie',
  'profile:Ambtenaar' => 'Ambtenaar',
  'profile:Werklokatie' => 'Werklokatie',
  'profile:Opleiding' => 'Opleiding',
  'profile:Competenties' => 'Competenties',
  'profile:categories:Werk' => 'Werk',
  'profile:categories:Persoonlijk' => 'Persoonlijk',
  'profile:categories:Portfolio' => 'Portfolio',
  'admin:plugins:info:groups' => '<p>Hiier kun je instellen wie de groep mag zien en wie er groepen aan mag maken..<span style="color: #993300;"> <a href="https://mijndeelsite.pleio.nl/pages/view/24129672/group" title="Lees hier meer...." target="_new">Lees hier meer....</a><a href="https://mijndeelsite.pleio.nl/pages/view/347478/google-analytics" target="_new" title="Lees hier meer...."></a></span></p>',
  'admin:plugins:info:newsletter' => 'Met deze plug-in kun je nieuwsbrieven versturen vanaf je deelsite!',
  'admin:plugins:info:csv_exporter' => '<p>Met deze plugin ben je in staat om content van je deelsite te exporteren. <span style="color: #993300;"><a href="https://mijndeelsite.pleio.nl/pages/view/26309641/csv-exporter" target="_new" title="Lees verder..."><span style="color: #993300;">Lees verder...</span></a></span></p>',
);
add_translation("nl", $language);
