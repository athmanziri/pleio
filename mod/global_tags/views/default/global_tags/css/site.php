<?php 
	// TODO: Cleanup when fixed in core. Temporary fix for keyboard selection of autocomplete results
?>
.ui-autocomplete .ui-menu-item {
	padding: 0px;
}

.ui-autocomplete a {
	width: 100%;
	display: inline-block;
	padding: 0 4px;
}

.ui-autocomplete .ui-state-hover {
	background-color: #eee;
}