<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\EnumType;
use Pleio\TypeRegistry;

class EventFilter extends EnumType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "EventFilter",
            "description" => "Show upcoming events or previous events",
            "values" => [
                "upcoming" => [ "value" => "upcoming" ],
                "previous" => [ "value" => "previous" ]
            ]
        ]);
    }
}
