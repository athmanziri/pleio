<?php
/**
 * Console access to the ELGG instance
 *
 * @package PleioConsole
 */

require_once('engine/lib/console.php');

if (!isset($argv)) {
    $argv = array();
}

pleio_console_start($argv);

// print a newline
echo PHP_EOL;