FROM ubuntu:16.04
EXPOSE 80

# Packages
RUN apt-get update && apt-get install --no-install-recommends -y \
        php7.0 \
        php-gd \
        php-xml \
        php-mysql \
        php-mbstring \
        ca-certificates \
        php-curl \
        php-bcmath \
        php-memcached \
        php-zip \
        telnet \
        inetutils-ping \
        apache2 \
        libapache2-mod-php7.0 \
        ssmtp \
        gettext \
        locales \
        && rm -rf /var/lib/apt/lists/*

# Scripts
COPY ./docker/run.sh /scripts/run.sh
COPY ./docker/initialize.sh /scripts/initialize.sh
RUN chmod -R 755 /scripts/*

# Configuration files
COPY ./docker/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY ./docker/remoteip.conf /etc/apache2/conf-available/remoteip.conf
COPY ./docker/php.ini /etc/php/7.0/apache2/php.ini
COPY ./docker/php.ini /etc/php/7.0/cli/php.ini
COPY ./docker/ssmtp.conf /etc/ssmtp/ssmtp.conf

# Apache configuration
RUN a2enmod rewrite remoteip
RUN a2enconf remoteip

# Web application
COPY . /app
COPY docker/settings.php /app/engine/settings.php

# Create data-folder
RUN mkdir /app-data && chown -R www-data:www-data /app-data

# Generate locales for Dutch and French
RUN locale-gen nl_NL fr_FR

# Define run script
CMD ["/scripts/run.sh"]
