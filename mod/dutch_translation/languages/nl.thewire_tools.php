<?php
$language = array (
  'thewire_tools:settings:wire_length' => 'Configureer de maximale lengte van een Wire post',
  'thewire_tools:settings:extend_activity' => 'Breid de activiteitenpagina uit met de mogelijkheid tweio\'s toe te voegen',
  'thewire_tools:login_required' => 'Je moet zijn aangemeld om deze functionaliteit te kunnen gebruiken',
  'thewire_tools:groups:tool_option' => 'Wil je gebruik maken van de mogelijkheid om tweio\'s (statusberichten) te plaatsen?',
  'thewire_tools:groups:error:not_enabled' => 'Tweio\'s zijn voor deze groep uitgeschakeld',
  'widgets:thewire:owner' => 'Van wie moeten de tweio-berichten worden getoond?',
  'widgets:thewire:filter' => 'Filter tweio-berichten (optioneel)',
  'thewire_tools' => 'Uitbreidingen van de tweio\'s',
  'thewire_tools:no_result' => 'Geen tweio\'s gevonden',
  'thewire_tools:menu:mentions' => 'Vermeldingen',
  'thewire_tools:settings:enable_group' => 'Schakel tweio\'s in voor groepen',
  'thewire_tools:settings:extend_widgets' => 'De tweio-widget uitbreiden met de optie om direct een bericht te kunnen plaatsen',
  'thewire_tools:notify:subject:new' => 'Nieuw wire bericht',
  'thewire_tools:notify:mention:subject' => 'Je bent genoemd in een tweio',
  'thewire_tools:notify:mention:message' => 'Beste %s,

%s heeft je genoemd in een tweio. 

Om de tweio te bekijken klik op onderstaande link:
%s

Vriendelijke groet,
Pleio',
  'thewire_tools:usersettings:notify_mention' => 'Ik wil een bericht ontvangen als ik word vermeld in een tweio',
  'thewire_tools:group:title' => 'Tweio\'s',
  'thewire_tools:search:title' => 'Zoek in de tweio\'s naar: \'%s\'',
  'thewire_tools:search:title:no_query' => 'Zoek in tweio\'s',
  'thewire_tools:search:no_query' => 'Om te zoeken in tweio\'s kun je hierboven een zoekterm invullen',
  'widgets:thewire_groups:title' => 'Tweio\'s in deze groep',
  'widgets:thewire_groups:description' => 'Toon de tweio\'s in deze groep',
  'widgets:index_thewire:title' => 'Tweio',
  'widgets:index_thewire:description' => 'Toon de laatste tweio\'s in de deelsite',
  'widgets:thewire_post:title' => 'Verstuur een tweio',
  'widgets:thewire_post:description' => 'Gebruik deze widget om een tweio te versturen',
);
add_translation("nl", $language);
