<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class ResendGroupInvitation {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "resendGroupInvitation",
            "description" => "Resend an invitation to join a group.",
            "inputFields" => [
                "id" => [
                    "type" => Type::int(),
                    "description" => "The id of the invitation to resend."
                ]
            ],
            "outputFields" => [
                "group" => [
                    "type" => $registry->get("Group"),
                    "resolve" => function($group) {
                        return Resolver::getEntity(null, $group, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $current_user = elgg_get_logged_in_user_entity();

        $annotation = get_annotation((int) $input["id"]);

        if ($annotation->name !== "email_invitation") {
            throw new Exception("could_not_find");
        }

        $group = $annotation->getEntity();
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find_group");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        $code = explode("|", $annotation->value);

        $site_url = elgg_get_site_url();

        $link = "{$site_url}groups/invitations/?invitecode={$code[0]}";

        $result = elgg_send_email(
            $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
            $code[1],
            "Herinnering om lid te worden van de groep {$group->name}",
            "Je bent nogmaals uitgenodigd door {$current_user->name} om lid te worden van de groep {$group->name}. Volg de onderstaande link om lid te worden van de groep:<br />
            <a href=\"{$link}\">$link</a>
            "
        );

        return [
            "guid" => $group->guid
        ];
    }
}
