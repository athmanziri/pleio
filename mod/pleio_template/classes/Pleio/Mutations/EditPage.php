<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditPage {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "editPage",
            "inputFields" => [
                "guid" => [ "type" => Type::nonNull(Type::string()) ],
                "title" => [ "type" => Type::string() ],
                "description" => [ "type" => Type::nonNull(Type::string()) ],
                "richDescription" => [ "type" => Type::string() ],
                "accessId" => [ "type" => Type::int() ],
                "tags" => [ "type" => Type::listOf(Type::string()) ]
            ],
            "outputFields" => [
                "entity" => [
                    "type" => $registry->get("Entity"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        $entity->title = $input["title"];

        $entity->description = $input["description"];
        $entity->richDescription = $input["richDescription"];

        if (isset($input["accessId"]) && $input["accessId"] != $entity->access_id) {
            $entity->access_id = $input["accessId"];

            foreach (Helpers::getChildren($entity, "row") as $child) {
                $child->access_id = $input["accessId"];
                $child->save();
                foreach (Helpers::getChildren($child, "page_widget") as $subchild) {
                    $subchild->access_id = $input["accessId"];
                    $subchild->save();
                }
            }
        }

        $entity->tags = filter_tags($input["tags"]);

        $result = $entity->save();

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
