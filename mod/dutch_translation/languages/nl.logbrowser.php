<?php
$language = array (
  'admin:administer_utilities:logbrowser' => 'Log browser',
  'logbrowser' => 'Log browser',
  'logbrowser:browse' => 'Bekijk systeemlogboek',
  'logbrowser:search' => 'Verfijn resultaten',
  'logbrowser:user' => 'Gebruikersnaam om op te zoeken',
  'logbrowser:starttime' => 'Begin tijd (bijvoorbeeld "last monday", "1 hour ago")',
  'logbrowser:endtime' => 'Eindtijd',
  'logbrowser:explore' => 'Verken logboek',
  'logbrowser:date' => 'Datum en tijd',
  'logbrowser:ip_address' => 'IP adres',
  'logbrowser:user:name' => 'Gebruiker',
  'logbrowser:user:guid' => 'Gebruiker GUID',
  'logbrowser:object' => 'Object type',
  'logbrowser:object:guid' => 'Object GUID',
  'logbrowser:action' => 'Actie',
  'logbrowser:no_result' => 'Geen resultaten',
);
add_translation("nl", $language);
