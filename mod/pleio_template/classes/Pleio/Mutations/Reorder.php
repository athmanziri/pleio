<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class Reorder {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "reorder",
            "inputFields" => [
                "guid" => [ "type" => Type::nonNull(Type::string()) ],
                "sourcePosition" => [ "type" => Type::nonNull(Type::int()) ],
                "destinationPosition" => [ "type" => Type::nonNull(Type::int()) ]
            ],
            "outputFields" => [
                "container" => [
                    "type" => Type::nonNull($registry->get("Entity")),
                    "resolve" => function($entity, array $args, $context, ResolveInfo $info) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $input["guid"] = (int) $input["guid"];

        $entity = get_entity($input["guid"]);
        if (!$entity || !$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        $options = [
            "type" => "object",
            "subtypes" => ["page", "wiki", "row"],
            "container_guid" => $entity->container_guid,
            "limit" => 100
        ];

        if ($entity->getSubtype() == "row") {
            $options["order_by"] = "e.guid";
        }

        $all_children = elgg_get_entities($options);
        if (!$all_children) {
            throw new Exception("could_not_save");
        }

        $all_children = Helpers::orderByManual($all_children);

        $ordered_guids = [];
        foreach ($all_children as $child) {
            $ordered_guids[] = $child->guid;
        }

        $new_ordered_guids = $ordered_guids;

        array_splice($new_ordered_guids, $input["sourcePosition"], 1);
        array_splice($new_ordered_guids, $input["destinationPosition"], 0, $ordered_guids[$input["sourcePosition"]]);

        foreach ($all_children as $child) {
            $new_order = array_search($child->guid, $new_ordered_guids);
            $child->order = $new_order;
            $child->save();
        }

        return [
            "guid" => $entity->container_guid
        ];
    }
}
