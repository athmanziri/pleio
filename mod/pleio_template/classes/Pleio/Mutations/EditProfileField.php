<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditProfileField {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "editProfileField",
            "inputFields" => [
                "guid" => [
                    "type" => Type::string()
                ],
                "accessId" => [
                    "type" => Type::int()
                ],
                "key" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "value" => [
                    "type" => Type::nonNull(Type::string())
                ],
            ],
            "outputFields" => [
                "user" => [
                    "type" => $registry->get("User"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $entity = get_entity(((int) $input["guid"]));
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!$entity instanceof \ElggUser) {
            throw new Exception("not_a_user");
        }

        $key = $input["key"];
        $value = $input["value"];

        if (!Helpers::isProfileFieldEditable($key)) {
            throw new Exception("field_not_editable");
        }

        $defaultFields = [
            [ "key" => "name", "name" => "Naam" ],
            [ "key" => "phone", "name" => "Telefoonnummer" ],
            [ "key" => "mobile", "name" => "Mobiel nummer" ],
            [ "key" => "emailaddress", "name" => "E-mailadres" ],
            [ "key" => "site", "name" => "Website" ],
            [ "key" => "aboutme", "name" => "Over mij" ]
        ];

        $customFields = elgg_get_plugin_setting("profile", "pleio_template") ? json_decode(elgg_get_plugin_setting("profile", "pleio_template"), true) : [];

        $allFields = array_merge($defaultFields, $customFields);

        if (!in_array($key, array_map(function($f) { return $f["key"]; }, $allFields))) {
            throw new Exception("invalid_key");
        }

        if ($value) {
            if ($key === "name") {
                $entity->$key = $value;
                $result = $entity->save();

                if ($result && elgg_is_active_plugin("pleio")) {
                    try {
                        $profile_handler = new \ModPleio\ProfileHandler($entity);
                        $profile_handler->changeName($value)->getBody()->getContents();
                    } catch (Exception $e) {
                        // silently fail
                    }
                }
            } else {
                $accessId = isset($input["accessId"]) ? $input["accessId"] : ACCESS_LOGGED_IN;
                $result = create_metadata($entity->guid, $key, $value, "", 0, $accessId, false, $site->guid);
            }
        } else {
            $entity->deleteMetadata($key);
            $result = true;
        }

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
