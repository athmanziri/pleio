<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\EnumType;
use Pleio\TypeRegistry;

class OrderDirection extends EnumType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "OrderDirection",
            "description" => "Configure the type of sorting, either ascending (asc) or descending (desc).",
            "values" => [
                "asc" => [ "value" => "asc" ],
                "desc" => [ "value" => "desc" ]
            ]
        ]);
    }
}
