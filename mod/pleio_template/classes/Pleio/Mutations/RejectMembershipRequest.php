<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class RejectMembershipRequest {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "rejectMembershipRequest",
            "description" => "Reject a membership request to a group.",
            "inputFields" => [
                "userGuid" => [
                    "type" => Type::int(),
                    "description" => "The guid of the user."
                ],
                "groupGuid" => [
                    "type" => Type::int(),
                    "description" => "The guid of the group."
                ]
            ],
            "outputFields" => [
                "group" => [
                    "type" => $registry->get("Group"),
                    "resolve" => function($group) {
                        return Resolver::getEntity(null, $group, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $logged_in_user = elgg_get_logged_in_user_entity();

        $group = get_entity($input["groupGuid"]);
        $user = get_entity($input["userGuid"]);

        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find_group");
        }

        if (!$user || !$user instanceof \ElggUser) {
            throw new Exception("could_not_find_group");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        remove_entity_relationship($user->guid, "membership_request", $group->guid);

        $result = elgg_send_email(
            $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
            $user->email,
            "Toegangsaanvraag voor de groep {$group->name} afgewezen",
            "De beheerder {$logged_in_user->name} heeft jouw aanvraag tot de groep {$group->name} afgewezen. Neem contact op met de beheerder voor meer informatie."
        );

        return [
            "guid" => $group->guid
        ];
    }
}
