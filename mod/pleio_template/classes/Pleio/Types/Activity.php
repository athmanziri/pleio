<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Pleio\Mapper;
use Pleio\TypeRegistry;

class Activity extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Activity",
            "fields" => [
                "guid" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "type" => [
                    "type" => Type::nonNull($registry->get("ActivityType")),
                ],
                "entity" => [
                    "type" => Type::nonNull($registry->get("Entity")),
                    "resolve" => function($activity, array $args, $context, ResolveInfo $info) {
                        return Mapper::getObject($activity["entity"], isset($activity["isHighlighted"]));
                    }
                ]
            ]
        ]);
    }
}

