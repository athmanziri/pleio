<?php
namespace Pleio\InputTypes;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class WidgetSettingInput extends InputObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "WidgetSettingInput",
            "fields" => [
                "key" => [ "type" => Type::string() ],
                "value" => [ "type" => Type::string() ]
            ]
        ]);
    }
}
