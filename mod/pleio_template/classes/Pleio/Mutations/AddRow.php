<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddRow {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "addRow",
            "inputFields" => [
                "layout" => [ "type" => Type::string() ],
                "containerGuid" => [ "type" => Type::string() ]
            ],
            "outputFields" => [
                "row" => [
                    "type" => $registry->get("Row"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $container = get_entity($input["containerGuid"]);
        if (!$container || $container->getSubtype() !== "page") {
            throw new Exception("could_not_find");
        }

        if (!$container->canEdit()) {
            throw new Exception("could_not_save");
        }

        $row = new \ElggObject();
        $row->subtype = "row";
        $row->layout = $input["layout"];
        $row->container_guid = $input["containerGuid"];
        $row->access_id = $container->access_id;
        $result = $row->save();

        if ($result) {
            return [
                "guid" => $row->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
