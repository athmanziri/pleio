<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditPassword {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "editPassword",
            "inputFields" => [
                "guid" => [
                    "type" => Type::string()
                ],
                "oldPassword" => [
                    "type" => Type::string()
                ],
                "newPassword" => [
                    "type" => Type::string()
                ]
            ],
            "outputFields" => [
                "user" => [
                    "type" => $registry->get("User"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $user = get_entity(((int) $input["guid"]));
        $oldPassword = $input["oldPassword"];
        $newPassword = $input["newPassword"];

        if (!$user) {
            throw new Exception("could_not_find");
        }

        if (!$user->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!$user instanceof \ElggUser) {
            throw new Exception("not_a_user");
        }

        if (elgg_is_active_plugin("pleio")) {
            $profile_handler = new \ModPleio\ProfileHandler($user);

            try {
                $body = $profile_handler->changePassword($oldPassword, $newPassword)->getBody();
                $response = json_decode($body);
            } catch (Exception $e) {
                throw new Exception("could_not_save");
            }

            if ($response->success) {
                $result = true;
            } else {
                throw new Exception($response->message);
            }
        } else {
            $credentials = array(
                "username" => $user->username,
                "password" => $oldPassword
            );

            try {
                pam_auth_userpass($credentials);
            } catch (\LoginException $e) {
                throw new Exception("invalid_old_password");
            }

            if (!validate_password($newPassword)) {
                throw new Exception("invalid_new_password");
            }

            $user->setPassword($newPassword);
            $user->code = "";

            if ($user->guid == elgg_get_logged_in_user_guid() && !empty($_COOKIE['elggperm'])) {
                // regenerate remember me code so no other user could
                // use it to authenticate later
                $code = _elgg_generate_remember_me_token();
                $_SESSION['code'] = $code;
                $user->code = md5($code);
                setcookie("elggperm", $code, (time() + (86400 * 30)), "/");
            }

            $result = $user->save();

            Helpers::sendPasswordChangeMessage($user);
        }

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
