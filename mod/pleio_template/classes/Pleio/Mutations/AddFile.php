<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddFile {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "addFile",
            "inputFields" => [
                "containerGuid" => [ "type" => Type::string() ],
                "file" => [ "type" => Type::string() ],
                "accessId" => [ "type" => Type::int() ],
                "writeAccessId" => [ "type" => Type::int() ],
            ],
            "outputFields" => [
                "entity" => [
                    "type" => $registry->get("Entity"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        if (!Helpers::isUser()) {
            if (Helpers::canJoin()) {
                Helpers::addUser();
            } else {
                throw new Exception("not_member_of_site");
            }
        }

        $file = Helpers::getFile($input["file"]);
        if (!$file) {
            throw new Exception("no_file");
        }

        if ($file["size"] === 0) {
            throw new Exception("invalid_filesize");
        }

        $entity = new \FilePluginFile();
        $entity->title = $file["name"];

        if (isset($input["accessId"])) {
            $entity->access_id = $input["accessId"];
        } else {
            $entity->access_id = get_default_access();
        }

        if (isset($input["writeAccessId"])) {
            $entity->write_access_id = $input["writeAccessId"];
        } else {
            $entity->write_access_id = ACCESS_PRIVATE;
        }

        if ($input["containerGuid"]) {
            $container = get_entity($input["containerGuid"]);
        }

        if ($container) {
            if ($container instanceof \ElggObject) {
                $entity->container_guid = $container->container_guid;
            } else {
                $entity->container_guid = $container->guid;
            }
        }

        $filestorename = elgg_strtolower(time() . basename($file["name"]));
        $entity->setFilename("file/" . $filestorename);
        $entity->originalfilename = $file["name"];

        $entity->open("write");
        $entity->close();

        move_uploaded_file($file["tmp_name"], $entity->getFilenameOnFilestore());

        $mime_type = \ElggFile::detectMimeType($entity->getFilenameOnFilestore(), $file["type"]);

        $entity->setMimeType($mime_type);
        $entity->simpletype = file_get_simple_type($mime_type);

        $result = $entity->save();

        if ($entity->simpletype == "image") {
            Helpers::generateThumbs($entity);
            $entity->icontime = time();
            $entity->save();
        }

        if ($container instanceof \ElggObject) {
            add_entity_relationship($container->guid, "folder_of", $entity->guid);
        }

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
