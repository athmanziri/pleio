<?php
elgg_register_event_handler("init", "system", "walled_garden_by_ip_init");
elgg_register_event_handler("init", "system", "walled_garden_by_ip_enforce", 1000);

function walled_garden_by_ip_init() {
	elgg_register_library("pgregg.ipcheck", dirname(__FILE__) . "/vendors/pgregg/ip_check.php");
	elgg_register_plugin_hook_handler("public_pages", "walled_garden", "walled_garden_by_ip_walled_garden_hook");
}

function walled_garden_by_ip_enforce() {
	if (PHP_SAPI === 'cli') {
		// do not check in cli mode
		return;
	}

	if (walled_garden_by_ip_validate_ip()) {
		return;
	}

	if (!elgg_is_logged_in()) {
		// hook into the index system call at the highest priority
		elgg_register_plugin_hook_handler('index', 'system', 'elgg_walled_garden_index', 1);

		$site = elgg_get_site_entity();
		if (!$site->isPublicPage()) {
			register_error(elgg_echo('loggedinrequired'));

			if (!elgg_is_xhr()) {
				forward('/?returnto=' . urlencode(full_url()));
			} else {
				forward();
			}
		}
	}
}

function walled_garden_by_ip_validate_ip() {
	$result = false;

	$client_ip = $_SERVER["REMOTE_ADDR"];
	$client_ip = elgg_trigger_plugin_hook("remote_address", "system", array("remote_address" => $client_ip), $client_ip);

	$allowed_ip = elgg_get_plugin_setting("allowed_ip", "walled_garden_by_ip");

	if(!empty($client_ip) && !empty($allowed_ip)){
		$allowed_ip = explode(PHP_EOL, $allowed_ip);

		// load validation library
		elgg_load_library("pgregg.ipcheck");

		foreach($allowed_ip as $check_ip){
			if(!empty($check_ip)){
				if ($result = ip_in_range($client_ip, $check_ip)) {
					// the IP address of the user is allowed, no point in searching any further
					break;
				}
			}
		}
	}

	return $result;
}

function walled_garden_by_ip_walled_garden_hook($hook, $type, $return_value, $params){
	$result = $return_value;

	if(!elgg_get_config("walled_garden")) {
		$result[] = "login";
	}

	return $result;
}
