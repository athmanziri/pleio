<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditGroupWidget {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "editGroupWidget",
            "inputFields" => [
                "guid" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "row" => [
                    "type" => Type::int()
                ],
                "col" => [
                    "type" => Type::int()
                ],
                "width" => [
                    "type" => Type::int()
                ],
                "settings" => [
                    "type" => Type::listOf($registry->get("WidgetSettingInput"))
                ]
            ],
            "outputFields" => [
                "entity" => [
                    "type" => $registry->get("Widget"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $entity = get_entity((int) $input["guid"]);
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if ($input["title"]) {
            $entity->title = $input["title"];
        }

        if ($input["row"]) {
            $entity->row = (int) $input["row"];
        }

        if ($input["col"]) {
            $entity->col = (int) $input["col"];
        }

        if ($input["width"]) {
            $entity->width = (int) $input["width"];
        }

        if ($input["settings"]) {
            $entity->setPrivateSetting("settings", json_encode($input["settings"]));
        }

        $result = $entity->save();

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
