<?php
global $CONFIG;
?>
<div class="form">
    <?php echo elgg_view("profile_manager/register/fields"); ?>

    <div class="buttons ___space-between">
        <?php echo elgg_view("input/submit", [
            "name" => "submit",
            "class" => "button",
            "value" => elgg_echo("pleio:request_access")
        ]); ?>

        <?php echo elgg_view("output/url", ["class" => "button ___link", "href" => $CONFIG->pleio->url . "action/logout", "text" => elgg_echo("logout")]); ?>
    </div>
</div>
