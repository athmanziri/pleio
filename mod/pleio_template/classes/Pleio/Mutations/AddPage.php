<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddPage {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "addPage",
            "inputFields" => [
                "title" => [ "type" => Type::string() ],
                "description" => [ "type" => Type::nonNull(Type::string()) ],
                "richDescription" => [ "type" => Type::string() ],
                "pageType" => [ "type" => Type::string() ],
                "containerGuid" => [ "type" => Type::int() ],
                "accessId" => [ "type" => Type::int() ],
                "tags" => [ "type" => Type::listOf(Type::string()) ]
            ],
            "outputFields" => [
                "entity" => [
                    "type" => $registry->get("Entity"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();

        $entity = new \ElggObject();
        $entity->subtype = "page";
        $entity->title = $input["title"];

        if (isset($input["accessId"])) {
            $entity->access_id = $input["accessId"];
        } else {
            $entity->access_id = get_default_access();
        }

        $entity->description = $input["description"];
        $entity->richDescription = $input["richDescription"];

        $entity->pageType = $input["pageType"];

        if (isset($input["containerGuid"])) {
            $entity->container_guid = $input["containerGuid"];
        } else {
            $entity->container_guid = $site->guid;
        }

        $entity->tags = filter_tags($input["tags"]);

        $result = $entity->save();

        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
