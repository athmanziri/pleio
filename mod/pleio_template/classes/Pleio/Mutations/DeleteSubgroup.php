<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class DeleteSubgroup {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "deleteSubgroup",
            "inputFields" => [
                "id" => [ "type" => Type::int() ]
            ],
            "outputFields" => [
                "success" => [ "type" => Type::boolean() ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $access_collection = get_access_collection($input["id"]);
        if (!$access_collection) {
            throw new Exception("could_not_find");
        }

        $group = get_entity($access_collection->owner_guid);
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        if ($group->subpermissions) {
            $subpermissions = unserialize($group->subpermissions);
        }

        if (!is_array($subpermissions)) {
            $subpermissions = array();
        }

        if (!in_array($input["id"], $subpermissions)) {
            throw new Exception("could_not_find");
        }

        if (delete_access_collection($access_collection->id)) {
            $subpermissions = array_diff($subpermissions, [$access_collection->id]);
            $group->subpermissions = serialize($subpermissions);
            $group->save();
        }

        return [
            "success" => true
        ];
    }
}
