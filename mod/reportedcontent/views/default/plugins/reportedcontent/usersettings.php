<?php 

	$user = elgg_get_page_owner_entity();
	$plugin = elgg_extract("entity", $vars);

	$noyes_options = array(
		"no" => elgg_echo("option:no"),
		"yes" => elgg_echo("option:yes")
	);

	if ($user->isAdmin()) {
		echo "<div>";
		echo elgg_echo("reportedcontent:usersettings:notify:description");
		echo "&nbsp;" . elgg_view("input/dropdown", array("name" => "params[notify]", "options_values" => $noyes_options, "value" => $plugin->getUserSetting("notify", $user->getGUID())));
		echo "</div>";
	}