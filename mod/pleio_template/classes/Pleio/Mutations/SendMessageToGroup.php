<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class SendMessageToGroup {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "sendMessageToGroup",
            "description" => "Send a message to the group members.",
            "inputFields" => [
                "guid" => [
                    "type" => Type::string(),
                    "description" => "The guid of the group to send the message to."
                ],
                "subject" => [
                    "type" => Type::string(),
                    "description" => "The subject of the message."
                ],
                "message" => [
                    "type" => Type::string(),
                    "description" => "The message to send."
                ],
                "isTest" => [
                    "type" => Type::boolean(),
                    "description" => "Is this a test message (send only to current user)"
                ],
                "recipients" => [
                    "type" => Type::listOf(Type::int()),
                    "description" => "An (optional) list of recipients to send the message to."
                ]
            ],
            "outputFields" => [
                "group" => [
                    "type" => $registry->get("Group"),
                    "resolve" => function($group) {
                        return Resolver::getEntity(null, $group, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        set_time_limit(0);

        $site = elgg_get_site_entity();

        $group = get_entity((int) $input["guid"]);
        if (!$group) {
            throw new Exception("could_not_find");
        }

        if (!$group->canEdit() || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_save");
        }

        if ($input["isTest"]) {
            $current_user = elgg_get_logged_in_user_entity();
            $result =   elgg_send_email(
                $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                $current_user->email,
                "Bericht van {$group->name}: {$input['subject']}",
                $input['message']
            );
        } else {
            $recipients = $input["recipients"];
            if ($recipients) {
                foreach ($recipients as $guid) {
                    if (!$group->isMember($guid)) {
                        continue;
                    }

                    $member = get_entity($guid);
                    if (!$member) {
                        continue;
                    }

                    $result = elgg_send_email(
                        $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                        $member->email,
                        "Bericht van {$group->name}: {$input['subject']}",
                        $input['message']
                    );
                }
            } else {
                foreach ($group->getMembers(0) as $member) {
                    $result = elgg_send_email(
                        $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                        $member->email,
                        "Bericht van {$group->name}: {$input['subject']}",
                        $input['message']
                    );
                }
            }
        }

        return [
            "guid" => $group->guid
        ];
    }
}
