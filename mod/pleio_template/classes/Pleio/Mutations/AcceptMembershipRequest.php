<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AcceptMembershipRequest {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "acceptMembershipRequest",
            "description" => "Accept a membership request to a group.",
            "inputFields" => [
                "userGuid" => [
                    "type" => Type::int(),
                    "description" => "The guid of the user."
                ],
                "groupGuid" => [
                    "type" => Type::int(),
                    "description" => "The guid of the group."
                ]
            ],
            "outputFields" => [
                "group" => [
                    "type" => $registry->get("Group"),
                    "resolve" => function($group) {
                        return Resolver::getEntity(null, $group, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $logged_in_user = elgg_get_logged_in_user_entity();

        $group = get_entity($input["groupGuid"]);
        $user = get_entity($input["userGuid"]);

        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find_group");
        }

        if (!$user || !$user instanceof \ElggUser) {
            throw new Exception("could_not_find_group");
        }

        if (!$group->canEdit()) {
            throw new Exception("could_not_save");
        }

        $relationship = check_entity_relationship($user->guid, "membership_request", $group->guid);
        if (!$relationship) {
            throw new Exception("could_not_find_membership_request");
        }

        groups_join_group($group, $user);
        remove_entity_relationship($user->guid, "membership_request", $group->guid);

        $link = Helpers::getURL($group, true);

        $result = elgg_send_email(
            $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
            $user->email,
            "Toegangsaanvraag voor de groep {$group->name} goedgekeurd",
            "De beheerder {$logged_in_user->name} heeft jouw aanvraag tot de groep {$group->name} goedgekeurd. Volg de onderstaande link om direct naar de groep te gaan:<br />
            <a href=\"{$link}\">$link</a>
            "
        );

        return [
            "guid" => $group->guid
        ];
    }
}
