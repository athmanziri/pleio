<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class Notification extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Notification",
            "fields" => [
                "id" => [ "type" => Type::int() ],
                "action" => [ "type" => Type::string() ],
                "performer" => [ "type" => $registry->get("User") ],
                "entity" => [ "type" => $registry->get("Entity") ],
                "container" => [ "type" => $registry->get("Entity") ],
                "timeCreated" => [ "type" => Type::string() ],
                "isUnread" => [ "type" => Type::boolean() ]
            ]
        ]);
    }
}
