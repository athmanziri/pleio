<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class AccessId extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "AccessId",
            "fields" => [
                "id" => [ "type" => Type::nonNull(Type::int()) ],
                "description" => [ "type" => Type::nonNull(Type::string()) ]
            ]
        ]);
    }
}
