<!-- Voeg issuenummer toe: beheer#123 -->
Gekoppeld aan issue:

## Samenvatting aanpassingen
<!-- Korte en bondige zin, of een puntenlijstje -->


## Afhankelijkheden
<!-- Externe afhankelijkheden? Of is het nodig om composer of yarn install te doen? -->


<!--
Checklist:
√ Voldoet deze MR aan de Definition of Done? (zie wiki)
√ Zijn de acceptatiecriteria van het issue opgelost?
√ Heb je getest in IE? ;P
-->
