<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Pleio\TypeRegistry;
use Pleio\Resolver;

class Row extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Row",
            "fields" => [
                "guid" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "layout" => [
                    "type" => Type::nonNull(Type::string())
                ],
                "canEdit" => [
                    "type" => Type::nonNull(Type::boolean())
                ],
                "widgets" => [
                    "type" => Type::listOf($registry->get("Widget")),
                    "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                        return Resolver::getWidgets($object);
                    }
                ]
            ]
        ]);
    }
}
