<?php
/**
 * Elgg etherpad widget
 *
 * @package etherpad
 */

$max = (int) $vars['entity']->max_display;
if(empty($max)){
	$max = 4;
}

$options = array(
	'type' => 'object',
	'subtype' => 'etherpad',
	'limit' => $max,
	'full_view' => FALSE,
	'pagination' => FALSE,
);

if(elgg_in_context("profile") || elgg_in_context("dashboard")){
	$options['owner_guid'] = $vars['entity']->owner_guid;
} elseif(elgg_in_context("groups")){
	$options['container_guid'] = $vars['entity']->container_guid;
}

$content = elgg_list_entities($options);

echo $content;

if ($content) {
	if(elgg_in_context("profile") || elgg_in_context("dashboard")){
		$url = "etherpad/owner/" . elgg_get_page_owner_entity()->username;
	} elseif(elgg_in_context("groups")){
		$url = "etherpad/group/" . elgg_get_page_owner_entity()->getGUID() . "/all";
	} else {
		$url = "etherpad/all";
	}
	
	$more_link = elgg_view('output/url', array(
		'href' => $url,
		'text' => elgg_echo('more'),
	));
	echo "<span class=\"elgg-widget-more\">$more_link</span>";
} else {
	echo elgg_echo('notfound');
}
