<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class ActivityList extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "ActivityList",
            "fields" => [
                "total" => [ "type" => Type::nonNull(Type::int()) ],
                "edges" => [ "type" => Type::listOf($registry->get("Activity")) ]
            ]
        ]);
    }
}
