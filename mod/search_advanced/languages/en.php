<?php

$english = array(
	'search_advanced:settings:combine_search_results' => "Combine search results",
	'search_advanced:settings:combine_search_results:info' => "This will show a combined list of the most recently created content based on your search query. There will be no more grouping on the 'all' page. Filtering with the side menu is still possible.",
	'search_advanced:settings:enable_multi_tag' => "Enable multi tag search",
	'search_advanced:settings:enable_multi_tag:info' => "If you use a comma in a search query it will be handled as two seperate search queries. The results will be combined. The comma seperator acts as an OR statement. This does not work for the autocomplete functionality.",
	'search_advanced:multisite:label' => "Search in",
	'search_advanced:multisite:current' => "current site",
	'search_advanced:multisite:mine' => "all my sites",
	'search_advanced:content:title' => "Content",
	'search_advanced:content:last_updated' => "Last updated",
	'search_advanced:content:created' => "Created",
	'search_advanced:widgets:search:edit:submit_behaviour' => "Submit behaviour",
	'search_advanced:widgets:search:edit:submit_behaviour:show_in_widget' => "Show in widget",
	'search_advanced:widgets:search:edit:submit_behaviour:go_to_search' => "Go to search",
	'search_advanced:settings:profile_fields' => "Configure profile field specific settings",
	'search_advanced:settings:profile_fields:field' => "Profile field",
	'search_advanced:settings:user_profile_fields:show_on_form' => "Show on search form",
	'search_advanced:settings:user_profile_fields:use_autocomplete' => "Search input via an autocomplete",
	'search_advanced:settings:user_profile_fields:info' => "Allow users to refine their search for users based on profile fields. Currently only text based fields are supported (text, location, url, etc).",
	'search_advanced:forms:search:user:autocomplete_info' => "Start typing and select from the list",
);
	
add_translation("en",$english);
