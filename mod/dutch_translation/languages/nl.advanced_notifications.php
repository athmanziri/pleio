<?php
$language = array (
  'advanced_notifications:settings:no_mail_content' => 'Wil je de content notificatie berichten vervangen met alleen een link naar de content?',
  'advanced_notifications:notification:email:body' => 'Om de content te bekijken, klik hier
%s',
  'advanced_notifications:cli:error:secret' => 'Opgegeven secret is niet geldig',
  'advanced_notifications:cli:error:type' => 'Opgegeven type is niet geldig',
  'advanced_notifications:settings:replace_site_notifications' => 'Wilt u de systeem meldingen, die normaal gesproken in de inbox verschijnen, vervangen door een gepersonaliseerde activiteiten pagina (gebaseerd op de meldingen instellingen)?',
  'advanced_notifications:activity:groups' => 'Mijn groepen',
  'advanced_notifications:activity:groups:title' => 'Activiteit in mijn groepen',
  'advanced_notifications:activity:groups:info' => 'Activiteit in groepen waar ik lid van ben',
  'advanced_notifications:activity:notifications' => 'Gepersonaliseerd',
  'advanced_notifications:activity:notifications:title' => 'Gepersonaliseerde activiteiten',
  'advanced_notifications:activity:notifications:info' => 'Activiteit gebaseerd op mijn meldingen instellingen',
  'advanced_notifications:discussion:create:subject' => 'Er is een discussie gestart %s',
  'advanced_notifications:discussion:reply:subject' => 'Reactie discussie: %s',
);
add_translation("nl", $language);
