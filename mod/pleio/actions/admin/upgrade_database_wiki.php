<?php
if(elgg_is_active_plugin("pleio_template")) {

    $dbprefix = elgg_get_config("dbprefix");
    $ia = elgg_set_ignore_access(true);

    $subtype_id = add_subtype("object", "wiki");

    $options = [
        "type" => "object",
        "subtypes" => ["page", ],
        "limit" => false
    ];

    foreach (elgg_get_entities($options) as $page) {
        if ($page->parent_guid) {
            $page->container_guid = $page->parent_guid;
            $page->save();
            update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_id} WHERE guid = {$page->guid}");
            _elgg_invalidate_memcache_for_entity($page->guid);
        }
    }
    $options = [
        "type" => "object",
        "subtypes" => ["page_top", ],
        "limit" => false
    ];

    foreach (elgg_get_entities($options) as $page) {
        update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_id} WHERE guid = {$page->guid}");
        _elgg_invalidate_memcache_for_entity($page->guid);
    }

    elgg_set_ignore_access($ia);

    system_message("Wiki geconverteerd voor nieuwe template");

    forward(REFERER);
}
