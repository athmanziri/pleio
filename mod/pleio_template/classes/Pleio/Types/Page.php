<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;
use Pleio\Resolver;

class Page extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Page",
            "interfaces" => [$registry->get("Entity")],
            "fields" => function() use ($registry) {
                return [
                    "guid" => [ "type" => Type::nonNull(Type::string()) ],
                    "status" => [ "type" => Type::int() ],
                    "canEdit" => [ "type" => Type::nonNull(Type::boolean()) ],
                    "title" => [ "type" => Type::string() ],
                    "description" => [ "type" => Type::string() ],
                    "hasChildren" => [
                        "type" => Type::boolean(),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::hasChildren($object);
                        }
                    ],
                    "children" => [
                        "type" => Type::listOf($registry->get("Page")),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::getChildren($object);
                        }
                    ],
                    "richDescription" => [ "type" => Type::string() ],
                    "excerpt" => [ "type" => Type::string() ],
                    "url" => [ "type" => Type::string() ],
                    "timeCreated" => [ "type" => Type::string() ],
                    "timeUpdated" => [ "type" => Type::string() ],
                    "accessId" => [ "type" => Type::int() ],
                    "pageType" => [ "type" => Type::string() ],
                    "rows" => [
                        "type" => Type::listOf($registry->get("Row")),
                        "resolve" => function($object, array $args, $context, ResolveInfo $info) {
                            return Resolver::getRows($object);
                        }
                    ],
                    "tags" => [ "type" => Type::listOf(Type::string()) ]
                ];
            }
        ]);
    }
}
