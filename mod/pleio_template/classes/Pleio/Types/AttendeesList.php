<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class AttendeesList extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "AttendeesList",
            "fields" => [
                "total" => [ "type" => Type::nonNull(Type::int()) ],
                "totalMaybe" => [ "type" => Type::nonNull(Type::int()) ],
                "totalReject" => [ "type" => Type::nonNull(Type::int()) ],
                "edges" => [ "type" => Type::listOf($registry->get("User")) ]
            ]
        ]);
    }
}
