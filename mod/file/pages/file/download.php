<?php
/**
 * Elgg file download.
 *
 * @package ElggFile
 */

// Get the guid
$file_guid = get_input("guid");

// Get the file
$file = get_entity($file_guid);
if (!$file) {
	register_error(elgg_echo("file:downloadfailed"));
	forward();
}

$filename = $file->originalfilename;
if (!$filename) {
	$pathinfo = pathinfo($file->getFilenameOnFilestore());
	if ($pathinfo) {
		$filename = $pathinfo['basename'];
	}
}

$mime = $file->getMimeType();
if (!$mime && $pathinfo) {
	switch ($pathinfo["extension"]) {
		case "jpg":
			$mime = "image/jpeg";
			break;
	}
}

$inlines = [
	"image/jpeg",
	"image/png",
	"application/pdf"
];

// fix for IE https issue
header("Pragma: public");

header("Content-type: $mime");
if (in_array($mime, $inlines)) {
	header("Content-Disposition: inline; filename=\"$filename\"");
} else {
	header("Content-Disposition: attachment; filename=\"$filename\"");
}

ob_clean();
flush();
readfile($file->getFilenameOnFilestore());
exit;
