<?php
if (webpack_dev_server_is_available()) {
    $path = "http://localhost:9001/web.css";
} else {
    $path = dirname(__FILE__) . "/../../../build/web.css";
}

$contents = file_get_contents($path);

$primary = elgg_get_plugin_setting("color_primary", "pleio_template");
if ($primary) {
    $contents = str_replace("#01689b", $primary, $contents);

    // --light
    $contents = str_replace("#3887ad", Pleio\Color::tint($primary, 0.6), $contents);
}

$secondary = elgg_get_plugin_setting("color_secondary", "pleio_template");
if ($secondary) {
    $contents = str_replace("#009ee3", $secondary, $contents);

    // --hover
    $contents = str_replace("#33b1e9", Pleio\Color::tint($secondary, 0.2), $contents);

    // --active
    $contents = str_replace("#0086c1", Pleio\Color::tint($secondary, 0.15), $contents);

    // --light
    $contents = str_replace("#99d8f4", Pleio\Color::tint($secondary, 0.6), $contents);

    // --lighter
    $contents = str_replace("#bfe7f8", Pleio\Color::tint($secondary, 0.75), $contents);

    // --lightest
    $contents = str_replace("#e6f5fc", Pleio\Color::tint($secondary, 0.9), $contents);

    // --bright
    $contents = str_replace("#f2fafe", Pleio\Color::tint($secondary, 0.95), $contents);

    // --highlight
    $contents = str_replace("#00c6ff", Pleio\Color::tint($secondary, 0.2), $contents);
}

$font = elgg_get_plugin_setting("font", "pleio_template");
if ($font) {
    switch ($font) {
        case "Roboto":
        case "Merriweather":
        case "Open Sans":
        case "Arial":
            $contents = str_replace("ROsanswebtext", $font, $contents);
    }
}

echo $contents;