<?php
$site = elgg_get_site_entity();
$logotime = $site->logotime ? $site->logotime : time();
?>
<p>
    <?php if ($site->logotime): ?>
        <img src="/mod/pleio_template/logo.php?lastcache=<?php echo $logotime; ?>" class="logo">
    <?php else: ?>
        <div class="logo">
            <?php echo elgg_echo("pleio_template:not_configured"); ?>
        </div>
    <?php endif; ?>
</p>

<p>
    <input type="file" name="logo" accept="image/*">
</p>

<?php if ($site->logotime): ?>
    <p>
        <input type="checkbox" id="remove_logo" name="remove_logo" value="1">
        <label for="remove_logo"><?php echo elgg_echo("pleio_template:remove_logo"); ?></label>
    </p>
<?php endif; ?>

<span class="elgg-subtext"><?php echo elgg_echo("pleio_template:logo:explanation"); ?></span>