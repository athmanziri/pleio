<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditAvatar {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "editAvatar",
            "inputFields" => [
                "guid" => [
                    "type" => Type::string()
                ],
                "avatar" => [
                    "type" => Type::string(),
                    "description" => "The string pointer to the file object."
                ],
            ],
            "outputFields" => [
                "user" => [
                    "type" => $registry->get("User"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $entity = get_entity(((int) $input["guid"]));
        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if ($input["avatar"]) {
            Helpers::editAvatar($input["avatar"], $entity);
            $entity->icontime = time();
        } else {
            unset($entity->icontime);
        }

        $result = $entity->save();
        if ($result) {
            return [
                "guid" => $entity->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
