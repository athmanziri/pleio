<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class AddEntity {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "addEntity",
            "inputFields" => [
                "type" => [ "type" => Type::nonNull($registry->get("Type")) ],
                "subtype" => [ "type" => Type::nonNull(Type::string()) ],
                "title" => [ "type" => Type::string() ],
                "description" => [ "type" => Type::nonNull(Type::string()) ],
                "richDescription" => [ "type" => Type::string() ],
                "isRecommended" => [ "type" => Type::boolean() ],
                "isFeatured" => [ "type" => Type::boolean() ],
                "featured" => [ "type" => $registry->get("FeaturedInput") ],
                "startDate" => [ "type" => Type::string() ],
                "endDate" => [ "type" => Type::string() ],
                "source" => [ "type" => Type::string() ],
                "location" => [ "type" => Type::string() ],
                "maxAttendees" => [ "type" => Type::string() ],
                "rsvp" => [ "type" => Type::boolean() ],
                "containerGuid" => [ "type" => Type::int() ],
                "accessId" => [ "type" => Type::int() ],
                "writeAccessId" => [ "type" => Type::int() ],
                "tags" => [ "type" => Type::listOf(Type::string()) ],
                "mentions" => [ "type" => Type::listOf(Type::string())],
            ],
            "outputFields" => [
                "entity" => [
                    "type" => $registry->get("Entity"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        if (!elgg_is_logged_in()) {
            throw new Exception("not_logged_in");
        }

        $site = elgg_get_site_entity();
        if (!Helpers::isUser()) {
            if (Helpers::canJoin()) {
                Helpers::addUser();
            } else {
                throw new Exception("not_member_of_site");
            }
        }

        if (!in_array($input["type"], array("object"))) {
            throw new Exception("invalid_type");
        }

        switch ($input["type"]) {
            case "object":
                if (!in_array($input["subtype"], array("file", "folder", "news", "blog", "question", "discussion", "comment","page", "wiki", "event", "task", "thewire", "poll"))) {
                    throw new Exception("invalid_subtype");
                }

                $entity = new \ElggObject();
                $entity->title = $input["title"];
                $entity->subtype = $input["subtype"];
            default:
                $entity->description = $input["description"];

                if ($input["richDescription"]) {
                    $entity->richDescription = $input["richDescription"];
                }

                if ((int) $input["containerGuid"]) {
                    $container = get_entity((int) $input["containerGuid"]);
                    if ($container instanceof \ElggGroup && $container->membership === ACCESS_PRIVATE && $container->group_acl) {
                        $defaultAccessId = $container->group_acl;
                    } elseif ($input["subtype"] === "comment") {
                        $defaultAccessId = $container->access_id;
                    } else {
                        $defaultAccessId = get_default_access();
                    }
                } else {
                    $defaultAccessId = get_default_access();
                }

                if (isset($input["accessId"])) {
                    $entity->access_id = (int) $input["accessId"];
                } else {
                    $entity->access_id = $defaultAccessId;
                }

                if (isset($input["writeAccessId"])) {
                    $entity->write_access_id = (int) $input["writeAccessId"];
                } else {
                    $entity->write_access_id = ACCESS_PRIVATE;
                }

                $entity->tags = filter_tags($input["tags"]);

                if (elgg_is_admin_logged_in()) {
                    if (isset($input["isRecommended"])) {
                        $entity->isRecommended = $input["isRecommended"];
                    }
                }

                if ($container) {
                    if ($input["subtype"] == "folder") {
                        if ($container instanceof \ElggGroup || $container instanceof \ElggUser) {
                            $entity->container_guid = $container->guid;
                            $entity->parent_guid = 0;
                        } else {
                            $entity->container_guid = $container->container_guid;
                            $entity->parent_guid = $container->guid;
                        }
                    } else {
                        $entity->container_guid = $container->guid;
                    }
                } else {
                    if ($input["subtype"] === "wiki") {
                        $entity->container_guid = $site->guid;
                    }
                }

                if (isset($input["mentions"]) && is_array($input["mentions"])) {
                    $entity->mentions = $input["mentions"];
                }
        }

        $result = $entity->save();

        if (!$result) {
            throw new Exception("could_not_save");
        }

        if (in_array($input["subtype"], ["news", "blog", "page", "event"])) {
            if (isset($input["isFeatured"])) {
                $entity->isFeatured = $input["isFeatured"];
            }

            if ($input["featured"]) {
                if (isset($input["featured"]["image"])) {
                    if ($input["featured"]["image"] === "false") {
                        unset($entity->featuredIcontime);
                    } else {
                        Helpers::saveToFeatured($input["featured"]["image"], $entity);
                        $entity->featuredIcontime = time();
                    }
                }

                if ($input["featured"]["video"]) {
                    $entity->featuredVideo = $input["featured"]["video"];
                } else {
                    unset($entity->featuredVideo);
                }

                if ($input["featured"]["positionY"]) {
                    $entity->featuredPositionY = $input["featured"]["positionY"];
                } else {
                    unset($entity->featuredPositionY);
                }
            }

            if ($input["source"]) {
                $entity->source = $input["source"];
            }

            $result = $entity->save();
        }

        if ($input["subtype"] === "event") {
            $startDate = strtotime($input["startDate"]);
            $endDate = strtotime($input["endDate"]);

            $entity->start_day = $startDate;
            $entity->start_time = $startDate;
            $entity->end_ts = $endDate;

            if ($input["location"]) {
                $entity->location = $input["location"];
            }

            if (isset($input["rsvp"])) {
                $entity->rsvp = $input["rsvp"];
            }

            if ($input["maxAttendees"]) {
                $entity->maxAttendees = $input["maxAttendees"];
            }

            $result = $entity->save();
        }

        if ($input["subtype"] === "comment") {
            update_entity_last_action($container->guid, $entity->time_created);
        }

        if ($input["subtype"] !== "comment") {
            add_entity_relationship(elgg_get_logged_in_user_guid(), "content_subscription", $entity->guid);
        }

        $view = "river/object/{$input["subtype"]}/create";
        add_to_river($view, 'create', elgg_get_logged_in_user_guid(), $entity->guid);

        if ($entity->mentions) {
            Helpers::notifyMentions($entity);
        }

        return [
            "guid" => $entity->guid
        ];
    }
}
