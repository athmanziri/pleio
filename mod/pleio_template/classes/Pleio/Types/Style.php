<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Pleio\TypeRegistry;

class Style extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "name" => "Style",
            "fields" => [
                "font" => [ "type" => Type::string() ],
                "colorPrimary" => [ "type" => Type::string() ],
                "colorSecondary" => [ "type" => Type::string() ],
                "colorHeader" => [ "type" => Type::string() ],
            ]
        ]);
    }
}
