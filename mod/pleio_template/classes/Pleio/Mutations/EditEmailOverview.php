<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditEmailOverview {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "editEmailOverview",
            "inputFields" => [
                "guid" => [ "type" => Type::string() ],
                "overview" => [ "type" => $registry->get("Overview") ]
            ],
            "outputFields" => [
                "user" => [
                    "type" => $registry->get("User"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();
        $entity = get_entity(((int) $input["guid"]));


        if (!$entity) {
            throw new Exception("could_not_find");
        }

        if (!$entity->canEdit()) {
            throw new Exception("could_not_save");
        }

        if (!$entity instanceof \ElggUser) {
            throw new Exception("not_a_user");
        }

        if (!in_array($input["overview"], ["daily", "weekly", "twoweekly", "monthly", "never"])) {
            throw new Exception("invalid_value");
        }

        if ($updates === "never") {
            $entity->removePrivateSetting("email_overview_{$site->guid}");
        } else {
            $entity->setPrivateSetting("email_overview_{$site->guid}", $input["overview"]);
        }

        return [
            "guid" => $entity->guid
        ];
    }
}
