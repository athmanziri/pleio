<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class JoinGroup {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "joinGroup",
            "description" => "Join a group. In the case of a closed group a membership request will be send, in the case of an open group the user will be joined immediately.",
            "inputFields" => [
                "guid" => [
                    "type" => Type::string(),
                    "description" => "The guid of the group to join."
                ]
            ],
            "outputFields" => [
                "group" => [
                    "type" => $registry->get("Group"),
                    "resolve" => function($group) {
                        return Resolver::getEntity(null, $group, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $site = elgg_get_site_entity();

        $group = get_entity((int) $input["guid"]);
        if (!$group || !$group instanceof \ElggGroup) {
            throw new Exception("could_not_find");
        }

        $user = elgg_get_logged_in_user_entity();
        if (!$user) {
            throw new Exception("not_logged_in");
        }

        if ($group->isPublicMembership() || $group->canEdit()) {
            groups_join_group($group, $user);
        } else {
            add_entity_relationship($user->guid, "membership_request", $group->guid);

            $owner = get_entity($group->owner_guid);
            $link = Helpers::getURL($group, true);

            $result = elgg_send_email(
                $site->email ? $site->email : "noreply@" . get_site_domain($site->guid),
                $owner->email,
                "Toegangsaanvraag voor de groep {$group->name}",
                "De gebruiker {$user->name} heeft toegang aangevraagd tot de {$group->name}. Volg de onderstaande link en ga via het menu Beheer naar toegangsaanvragen om de aanvraag te beoordelen:<br />
                <a href=\"{$link}\">$link</a>
                "
            );
        }

        return [
            "guid" => $group->guid
        ];
    }
}
