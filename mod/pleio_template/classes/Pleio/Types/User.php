<?php
namespace Pleio\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Pleio\TypeRegistry;
use Pleio\Resolver;

class User extends ObjectType {
    public function __construct(TypeRegistry $registry) {
        parent::__construct([
            "interfaces" => [ $registry->get("Entity") ],
            "fields" => [
                "guid" => [ "type" => Type::nonNull(Type::string()) ],
                "status" => [ "type" => Type::int() ],
                "username" => [ "type" => Type::string() ],
                "name" => [ "type" => Type::string() ],
                "email" => [
                    "type" => Type::string(),
                    "resolve" => function($user, array $args, $context, ResolveInfo $info) {
                        return Resolver::getEmail($user);
                    }
                ],
                "emailNotifications" => [
                    "type" => Type::boolean(),
                    "resolve" => function($user, array $args, $context, ResolveInfo $info) {
                        return Resolver::getEmailNotifications($user);
                    }
                ],
                "getsNewsletter" => [
                    "type" => Type::boolean(),
                    "resolve" => function($user, array $args, $context, ResolveInfo $info) {
                        return Resolver::getsNewsletter($user);
                    }
                ],
                "emailOverview" => [
                    "type" => $registry->get("Overview"),
                    "resolve" => function($user, array $args, $context, ResolveInfo $info) {
                        return Resolver::emailOverview($user);
                    }
                ],
                "profile" => [
                    "type" => Type::listOf($registry->get("ProfileItem")),
                    "resolve" => function($user, array $args, $context, ResolveInfo $info) {
                        return Resolver::getProfile($user);
                    }
                ],
                "stats" => [
                    "type" => Type::listOf($registry->get("StatsItem")),
                    "resolve" => function($user, array $args, $context, ResolveInfo $info) {
                        return Resolver::getStats($user);
                    }
                ],
                "tags" => [ "type" => Type::listOf(Type::string()) ],
                "icon" => [ "type" => Type::string() ],
                "url" => [ "type" => Type::string() ],
                "canEdit" => [ "type" => Type::boolean() ]
            ]
        ]);
    }
}
