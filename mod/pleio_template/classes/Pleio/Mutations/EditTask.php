<?php
namespace Pleio\Mutations;

use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use Pleio\Helpers;
use Pleio\Resolver;
use Pleio\Exception;

class EditTask {
    public static function getMutation($registry) {
        return Relay::mutationWithClientMutationId([
            "name" => "editTask",
            "inputFields" => [
                "guid" => [ "type" => Type::string() ],
                "state" => [ "type" => Type::string() ]
            ],
            "outputFields" => [
                "entity" => [
                    "type" => $registry->get("Task"),
                    "resolve" => function($entity) {
                        return Resolver::getEntity(null, $entity, null);
                    }
                ]
            ],
            "mutateAndGetPayload" => function($input) {
                return self::mutate($input);
            }
        ]);
    }

    static function mutate($input) {
        $task = get_entity((int) $input["guid"]);
        if (!$task) {
            throw new Exception("could_not_find");
        }

        if (!$task->canEdit() || !$task instanceof \ElggObject || $task->getSubtype() !== "task") {
            throw new Exception("could_not_save");
        }

        $task->state = $input["state"];

        $result = $task->save();

        if ($result) {
            return [
                "guid" => $task->guid
            ];
        }

        throw new Exception("could_not_save");
    }
}
